<?php 



/**
 * @file stormticket_assignment_jsapp.theme.inc
 */

/**
 * theme of the persons list
 */
function theme_stormticket_assignment_jsapp_persons($persons) {
  $content = '';
  
  $content .= '<ul id="jsapp-persons">';
  foreach ($persons as $pnid => $person) {
    $content .= '<li class="jsapp-person" id="id_person_'. $pnid .'">'. $person['title'];
    
      $odd_even = TRUE;
      $content .= '<ul class="jsapp-person-tickets connected-sortable droppable has-tickets" id="id_personlist_'. $pnid .'">';
      foreach ($person['tickets'] as $tnid => $ticket) {
        $content .= theme('stormticket_assignment_jsapp_ticket', $tnid, 'persons', $odd_even);
        $odd_even = !$odd_even;
      }      
      $content .= '</ul>';
    
    $content .= '</li>';
  }
  $content .= '</ul>';
  
  return $content;
}

/**
 * theme of the tickets list
 */
function theme_stormticket_assignment_jsapp_tickets($tickets) {
  $content = '';
  
  $odd_event = TRUE;
  $content .= '<ul id="jsapp-tickets" class="has-tickets">';
  foreach ($tickets as $tnid => $ticket) {
    $content .= theme('stormticket_assignment_jsapp_ticket', $tnid, 'tickets', $odd_even);
    $odd_even = !$odd_even;
  }
  $content .= '</ul>';
  
  return $content;
}

/**
 * theme of a single ticket entry
 * @param string $type - attribute is needed for truely unique IDs since a ticket may be 
 * already assigned to paresn (left column) and filtered in the ticket list (right column)
 */
function theme_stormticket_assignment_jsapp_ticket($tnid, $type, $odd_even) {
  $ticket = node_load($tnid);
  
  $tiles = array();

  $tiles[] = '<span class="ctrl-element cmd-dd"></span>';
  
  $title = strlen($ticket->title) > 65 ? substr($ticket->title, 0, 35) .' ... '. substr($ticket->title, -25) : $ticket->title;
  $tiles[] = '<span class="has-tooltip block-overview-trigger" title="'. $ticket->title . '">'. strip_tags($title) .'</span>';
  $tiles[] = ' ';
  $tiles[] = l(t('view'), 'node/'. $ticket->nid, array('attributes' => array('class' => 'external', 'target' => '_blank')));

  $overview  = '<h4>'. $ticket->title .'</h4>';
  $overview .= '<div class="overview-ctrl-elements">';
    $overview .= drupal_get_form('stormticket_assignment_jsapp_single_ticket_overview_form', $ticket);  
  $overview .= '</div>';
  $overview .= '<div class="overview-body">'. $ticket->body .'</div>';
  
  $tiles[] = '<div class="block-overview container-inline">'. $overview .'</div>';
  
  $tiles[] = '<div class="ctrl-elements">';
  $tiles[] = '<span class="ctrl-element hidden-element cmd-save"></span>'; 
  $tiles[] = '<span class="ctrl-element hidden-element cmd-ajax"></span>';
  $tiles[] = '</div>'; 
  
  $odd_even = $odd_even ? 'odd' : 'even';
  
//  // adding additional elements --> sync w/ stormticket_assignment_jsapp_single_ticket_form()
//  
//  $data = array();
//  $data['tnid'] = $ticket->nid;
//  $data['pnid'] = $ticket->assigned_nid;
//  $data['datebegin'] = $ticket->datebegin;
//  $data['dateend'] = $ticket->dateend;
//  $data['duration'] = $ticket->duration;
//  $data['durationunit'] = $ticket->durationunit;
//  $data['ticketpriority'] = $ticket->ticketpriority;
//  $data = json_encode($data);
//  $data = str_replace('"', "'", $data);
//  
//  $tiles[] = '<input type="hidden" name="jsapp_data" value="('. $data .')" />';
  
  return '<li class="jsapp-single-ticket '. $odd_even .'" id="id_'. $type .'_'. $tnid .'">'. implode('', $tiles) .'</li>';
}



