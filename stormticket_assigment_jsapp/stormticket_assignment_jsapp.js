
/**
 * @desc adds JS magic
 * 
 * @author Nikola Ivanov <nikola.ivanov@cocomore.com>
 */

var storm = storm || {};
storm.contrib = storm.contrib || {};

/**
 * @param object jQuery 1.4.2 
 */
storm.contrib.ta_jsapp = (function($) {
  // exposable public API
  var _publicAPI = {};
  
  /**
   * helpers and commons
   */
  
  /**
   * get the ID of an <li> item
   */
  var _get_item_id = function($item) {
    var res = $item.attr('id').match(/(.+)[-=_](.+)[-=_](.+)/);
    res = {'prefix': res[1], 'type': res[2], 'id': res[3]};
    return res;
  };
  
  /**
   * notify the user using the gitter jQuery plugin
   */
  var _notify = function(title, msg, sticky) {
    // @todo add additional options
    // @todo see if gitter is available and alter if not
    $.gritter.add({
      title: title,
      text: msg,
      sticky: sticky
    });    
  };  
  
  /**
   * init and prevent double init
   */
  var _inited = false;
  var _init = function() {
    if (!_inited) {
      // set dims of containers
      // #ta_jsapp_p and #ta_jsapp_t have the same topOffset
      // boldly assume neither filter fieldset is expanded
      // @todo @fixme scrolling is not possible accross containers whiel dragging
      // see http://forum.jquery.com/topic/scrolling-container-while-dragging
      // possible solution: some auto scroll plugin
      // but for now: disable
      // $('#ta_jsapp_p, #ta_jsapp_t').height($(window).height() - $('#ta_jsapp_p').offset().top);
      
      // hide submit buttons
      $('input.hidden-button').hide();
      
      // init DD
      _attachDD();
      
      // init dialog and datepicker
      $('div.block-overview', '#ta_jsapp_p').each(function(i) {
        // overviews in the persons column
        _attachDatepicker.apply(this);
        _attachDialog.apply(this);
      });
      $('span.block-overview-trigger', '#ta_jsapp_p').css('cursor', 'pointer').click(function(){
        var dialog_id = $(this).parent().data('jsapp_dialog');
        $('#'+ dialog_id).dialog('open');
      });
      $('div.block-overview', '#ta_jsapp_t').each(function(i) {
        // overviews int the tickets column
        // 1) ticket has twin -> open the dialog of the twin, remove this
        // 2) ticket has no twin -> init the dialog
        var $item = $(this).parent(), id = _get_item_id($item), $twin = $('#id_persons_'+ id.id);
        if ($twin.length) {
          // item has a twin, remove the element
          $(this).remove();
        }
        else {
          // item has no twin
          _attachDatepicker.apply(this);
          _attachDialog.apply(this);
        }
      });
      $('span.block-overview-trigger', '#ta_jsapp_t').css('cursor', 'pointer').click(function(){
        var $item = $(this).parent(), id = _get_item_id($item), $twin = $('#id_persons_'+ id.id), dialog_id;
        if ($twin.length) {
          // open the dialog of the twin
          dialog_id = $twin.data('jsapp_dialog');
        }
        else {
          // open the self dialog
          dialog_id = $item.data('jsapp_dialog');          
        }
        $('#'+ dialog_id).dialog('open');
      });
      
      // set backup values on all overview input elements
      $('input[type!=hidden], select', 'div.overview-ctrl-elements').each(function(i) {
        $(this).data('jsapp_backup_value', $(this).val());
      });    

      // bind behaviors
      $('span.cmd-save', '#ta_jsapp').click(function() {
          var $item = $(this).parents('li.jsapp-single-ticket'), id = _get_item_id($item), $twin, that = $item.get(0);
          if (id.type == 'tickets') {
            $twin = $('#id_persons_'+ id.id);
            if ($twin.length > 0) {
              that = $twin.get(0);
            }
          }
          storm.contrib.ta_jsapp.save_item.apply(that);
        });

      // yep
      _inited = true;
    }
  };
  _publicAPI.init = _init;  
  
  /**
   * attach DD
   */
  var _attachDD = function() {
    // init draggables
    $('li.jsapp-single-ticket').draggable({
      appendTo: 'body',
      handle: 'span.cmd-dd',
      cursor: 'crosshair',
      helper: 'clone',
      revert: 'invalid',
      start: function(event, ui) {
        /**
         * starting to drag a draggable <li> item
         * - prevent dropping the item in its container
         * - prevent other droppables that probably already contain the ticket to accept it
         */
        var $item = $(this);
        var id = _get_item_id($item);
        var disabled_droppables = [];
        var $twin, $parent;
        
        if (id.type == 'tickets') {
          // item originates from the tickets column
          // see if item has a twin and disable the droppable
          $twin = '#id_persons_'+ id.id;
          $twin = $($twin);
          if ($twin) {
            $parent = $twin.parent();      
          }
          else {
            // no twin item
            // --> nothing to disable
            $parent = [];
          }
        }
        else {
          // item originates from the persons columng
          // --> disable the parent droppable of the item
          $parent = $item.parent();
        }
        if ($parent.length) {
          $parent.droppable('disable').addClass('may-not-accept');
          disabled_droppables.push($parent.attr('id'));
        }
        $item.data('jsapp_dd_disabled', disabled_droppables);      
      },
      stop: function(event, ui) {
        /**
         * stopping to drag a draggable <li> item
         * - re-enable all disabled droppables and clear data
         */
        var $item = $(this);
        
        $.each($item.data('jsapp_dd_disabled'), function(i, n){
          $('#'+ n).droppable('enable').removeClass('may-not-accept');
        });
        $item.data('jsapp_dd_disabled', []);
      }
    });
    
    // init droppables
    $('ul.droppable').droppable({
      accept: 'li.jsapp-single-ticket',
      drop: function(event, ui) {
        /**
         * dropping an <li> item over an <ul> droppable
         * - append the item
         * - see if item is present in other lists --> remove it from there
         * - change id if nessessery
         * - trigger assignee save
         */
        var $item = $(ui.draggable[0]), $dialog, $twin;
        var id = _get_item_id($item);
        
        if (id.type == 'tickets') {
          // item originates from the tickets column
          // --> clone the item
          $twin = $('#id_persons_'+ id.id);
          if ($twin.length) {
            // item is already assigned to another person visible in the persons column
            // --> move the item around
            $item = $twin;
          }
          else {
            // item is not assined to any visible person or item is not assigned at all
            
            // @todo @fixme item should be cloned and the clone should be appended
          }
        }
        else {
          // item originates from the persons column
          // --> just move it
        }
        $item.appendTo(this);

        // refresh assignee nid
        $dialog = $('#'+ $item.data('jsapp_dialog'));
        id = _get_item_id($(this));
        $('input[name=assigned_nid]', $dialog).val(id.id);
        
        // sort list and fix zebra
        $('#'+ this.id +'>li')
          .tsort('span.has-tooltip', {returns: true})
          .removeClass('odd even')
          .filter(':odd').addClass('odd').end()
          .filter(':even').addClass('even');
        
        _ajaxQueue.push($item);
        _ajax();
      }
    });      
  };
  
  /**
   * attach calendar widget
   */
  var _attachDatepicker = function() {
    var dates = $('input[name=datebegin], input[name=dateend]', this).datepicker({
      changeMonth: true,
      numberOfMonths: 3,
      showWeek: true,
      firstDay: 1,
      dateFormat: 'yy-mm-dd',
      onSelect: function(selectedDate) {
        var option = this.name == "datebegin" ? "minDate" : "maxDate";
        var instance = $(this).data("datepicker");
        var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
      }
    });
  };
  
  /**
   * attach dialog widget
   */
  var _attachDialog = function() {
    var $dialog = $(this), $item = $dialog.parents('li.jsapp-single-ticket'), id = _get_item_id($item), i18nbuttons = {};
    
    // workaround to provide i18n of button labels
    i18nbuttons[Drupal.t('Save')] = function() {
      this.is_save_button = true;
      $(this).dialog('close');
    };
    i18nbuttons[Drupal.t('Cancel')] = function() {
      this.is_save_button = false;
      $(this).dialog('close');
    };                 
                
    $dialog.dialog({
      modal: true,
      autoOpen: false,
      width: 800,
      open: function(event, ui) {
        var title = $('h4', $(this)).css('display', 'none').html();
        $(this).dialog('option', 'title', title);
      },
      close: function(event, ui) {
        if (!event.target.is_save_button) {
          // <cancel> button, dialog closed on <X> or <esc> key pressed
          $('input[type!=hidden], select', this).val(function(index, value) { 
            return $(this).data('jsapp_backup_value'); 
          });
        }
        else {
          // <save> button is clicked
          _ajaxQueue.push($('#'+ $(this).data('jsapp_item')));
          _ajax();
        }
        // clear helper
        event.target.is_save_button = false
      },
      buttons: i18nbuttons
    });
    $dialog.attr('id', 'id_dialog_'+ id.id);
    $dialog.data('jsapp_item', $item.attr('id'));
    
    $item.data('jsapp_dialog', 'id_dialog_'+ id.id);
  };

  /**
   * handle AJAX requests
   */
  var _ajaxRunning = false;
  var _ajaxQueue = []; 
  var _ajax = function() {
    var $item, id, $dialog, options;

    if (!_ajaxRunning && _ajaxQueue.length > 0) {
      $item = _ajaxQueue.shift();
      id = _get_item_id($item);
      $dialog = $('#'+ $item.data('jsapp_dialog'));

      options = {
        type: "POST",
        cache: false,
        url: $('#stormticket-assignment-jsapp-single-ticket-form').attr('action'),
        data: $('form', $dialog).serialize(),
        dataType: 'JSON',
        beforeSend: function(xhr) {
          _ajaxRunning = true;
          $('span.cmd-ajax', '#id_persons_'+ id.id +', #id_tickets_'+ id.id).css('visibility', 'visible');
        },
        complete: function(xhr, status) {
          $('span.cmd-ajax', '#id_persons_'+ id.id +', #id_tickets_'+ id.id).css('visibility', 'hidden');
          _ajaxRunning = false;
          _ajax();
        },
        success: function(data, status, xhr) {
          var msg = [], sticky;
          data = eval('('+ data +')');
          
          // render all msgs
          if (data.msgs.length) {
            $.each(data.msgs, function (k, v) {
              msg.push(v);
            });            
          }
          
          if (data.status == 'success') {
            sticky = false;
            $item.data('jsapp_saved', true);
            $('span.cmd-save', '#id_persons_'+ id.id +', #id_tickets_'+ id.id).css('visibility', 'hidden');
            if (!msg.length) {
              msg.push(' ');
            }
          }
          else if (data.status == 'error') {
            sticky = true;
            $item.data('jsapp_saved', false);
            $('span.cmd-save', '#id_persons_'+ id.id +', #id_tickets_'+ id.id).css('visibility', 'visible');
            
            // render errors
            $.each(data.errors, function (k, v) {
              $('input[name='+ k +']', $dialog).addClass('error');
              msg.push(v);
            });
          }
          else { // data.status = notfound etc.
            sticky = true;
            $item.data('jsapp_saved', false);
            $('span.cmd-save', '#id_persons_'+ id.id +', #id_tickets_'+ id.id).css('visibility', 'visible');
            msg = data.msgs;
          }
          
          if (data.cmd) {
            // append additional commands
          }
          
          _notify(data.title, msg.join('<br/>'), sticky);          
        },
        error: function(xhr, status, error) {
          // something went wrong
          // --> notify the user to manualy click the save button
          // --> show the save button
          $('span.cmd-save', '#id_persons_'+ id.id +', #id_tickets_'+ id.id).css('visibility', 'visible');
          _notify(Drupal.t('Error with item ') +'<em>'+ $('span.has-tooltip', $item).html() +'</em>', Drupal.t('Item couldn\'t be saved due to network problems. Please try again later.'), true);
          $item.data('jsapp_saved', false);
        }
      };
      $.ajax(options);
    }
  };
  // _publicAPI.ajax = _ajax;
  
  /**
   * behaviors callbacks
   */
  var _save_item = function() {
    _ajaxQueue.push($(this));
    _ajax();
  };
  _publicAPI.save_item = _save_item;
  
  return _publicAPI;
}(jQuery));

Drupal.behaviors.storm_contrib_ta_jsapp = function(context) {
  storm.contrib.ta_jsapp.init();
};