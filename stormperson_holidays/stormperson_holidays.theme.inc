<?php

/**
 * @file stormperson_holidays.theme.inc
 *
 * theming functions for stormperson holidays
 */


/**
 * theme person page view - display holidays
 *
 * @param $node
 * @return unknown_type
 */
function theme_stormperson_holidays_display_person_holidays($node, $has_holiday =  FALSE, $date = 0) {
  $content = '';

  $content .= '<div class="stormbody stormperson-holidays">';
  $content .= '<strong>';
  $content .= t('Holidays');
  $content .= '</strong>';
  $content .= '</div>';

  if ($has_holiday) {
    // PERSON HAS HOLIDAY ON SELECTED DATE
    $content .= theme('stormperson_holidays_person_has_holiday', $node, $has_holiday, $date, $display_small);
//    $content .= '<div class="stormbody stormperson-has-holiday" style="color:red;">';
//    $content .= t('Person has holiday on %value', array('%value' => storm_contrib_common_time_to_date($has_holiday, storm_contrib_common_get_custom_display_date_format())));
//    $content .= '</div>';
  }

  // TOTAL NUMBER OF DAYS
  $content .= '<div class="stormbody stormperson-holidays-number-of-holidays-total">';
  $content .= theme('storm_view_item', t('total days'), theme('storm_contrib_common_number_format', $node->stormperson_holidays_number_holidays_total_number_holidays) .' '. t('days'));
  $content .= '</div>';

  // NUMBER OF DAYS PER YEAR
  $content .= '<div class="stormbody stormperson-holidays-number-of-holidays-per-year">';
  $content .= theme('storm_view_item', t('Days per year'), theme('storm_contrib_common_number_format', $node->stormperson_holidays_number_holidays_per_year) .' '. t('days'));
  $content .= '</div>';

  // HOLIDAYS REMAINING FROM LAST YEAR
  $content .= '<div class="stormbody stormperson-holidays-number-of-holidays-remaining-last-year">';
  $content .= theme('storm_view_item', t('Days last year'), theme('storm_contrib_common_number_format', $node->stormperson_holidays_number_holidays_remaining_last_year) .' '. t('days'));
  $content .= '</div>';

  // HOILDAYS TOOK THIS YEAR
  $content .= '<div class="stormbody stormperson-holidays-number-holidays-took">';
  $content .= theme('storm_view_item', t('permitted'), theme('storm_contrib_common_number_format', $node->stormperson_holidays_holidays_took['number_holidays']) .' '. t('days'));
  $content .= '</div>';


  // HOLIDAYS STILL AVAILABLE
  $content .= '<div class="stormbody stormperson-holidays-number-holidays-available">';
  $content .= theme('storm_view_item', t('still available'), theme('storm_contrib_common_number_format', $node->stormperson_holidays_number_holidays_available) .' '. t('days'));
  $content .= '</div>';

  return $content;
}


/**
 * theme if a person has holiday on selected day
 *
 * @param unknown_type $node
 * @param unknown_type $has_holiday
 * @return unknown_type
 */
function theme_stormperson_holidays_person_has_holiday($node, $has_holiday, $date, $display_small = FALSE) {
  $content = '';

  if (!empty($node) && !empty($date)) {
    if ($display_small) {
      $content .= '<div class="stormperson-holidays-person-has-holiday" style="color:red;">'. t('has holiday on %value', array('%value' => $date)) .'</div>';
    }
    else {
      $content .= '<div class="stormbody stormperson-has-holiday" style="color:red;">';

      switch ($has_holiday) {

        case 'half_day_holiday':
          $content .= t('Person has a half day holiday on %value', array('%value' => $date));
          break;

        case 'full_day_holiday':
          $content .= t('Person has holiday on %value', array('%value' => $date));
          break;

      }

      $content .= '</div>';
    }
  }
  return $content;
}


/**
 * theme holiday edit form
 *
 * @param $form
 * @return unknown_type
 */
function theme_stormperson_holidays_person_holidays_edit_form($form) {

  $content = '';

  $content .= drupal_render($form['person_holiday_results']);

  $content .= '<div class="clear"></div>';

  $content .= drupal_render($form['allocate_holiday']);

  $content .= '<div class="clear"></div>';

  $content .= '<p>&nbsp;</p>';

  $content .= drupal_render($form['already_allocated_holidays']);

  $content .= drupal_render($form);

  return $content;
}


/**
 * display all holidays
 *
 * @param unknown_type $holidays
 * @return unknown_type
 */
function theme_stormperson_holidays_display_holidays($holidays, $start_time, $end_time, $mode) {

  if (empty($_SESSION['storm_exports_export_'. request_uri() ])) {
    drupal_add_css(drupal_get_path('module', 'stormperson_holidays')."/stormperson_holidays.css");

    $content = '';

    switch ($mode) {
      case 'year':
        $year = date("Y", $start_time);
        $content .= '<div class="stormperson-holidays-heading-prev">'.l('<<', "storm/holidays/display/".($year-1)).'</div>';
        $content .= '<div class="stormperson-holidays-heading-next">'.l('>>', "storm/holidays/display/".($year+1)).'</div>';
        $content .= '<div class="stormperson-holidays-heading"><h3>'.$year.'</h3></div>';

        for ($month=1; $month<=12; $month++) {
          $content .= '<div class="stormperson-holidays-calendar-year-month month-'.$month.'">';
          $start_time_month = mktime(0,0,0,$month, 1, $year);
          $end_time_month = mktime(0,0,0,$month, date("t", $start_time_month), $year);
          $content .= theme('stormperson_holidays_display_holidays_month', $holidays, $start_time_month, $end_time_month, $mode);
          $content .= '</div>';
        }

        break;
      case 'month':
      case 'week':
        $content .= theme('stormperson_holidays_display_holidays_month', $holidays, $start_time, $end_time, $mode);
        break;
    }
  }

  // EXPORT
  if (module_exists('storm_exports')) {

    // IF EXPORT BUTTON WAS PRESSED
    if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {

      $header = array(
      array('data' => t('Date')),
      array('data' => t('Person')),
      );

      $rows = array();

      if (!empty($holidays)) {
        foreach ($holidays as $month => $month_array) {

          if (!empty($rows)) {
            $row = array();
            $row[] = array('data' => ' ', 'colspan' => 4);
            $rows[] = array('data' => $row);
          }

          $row = array();
          $row[] = array('data' => '<strong>'. $month .'</strong>', 'colspan' => 4);
          $rows[] = array('data' => $row);

          if (!empty($month_array)) {
            foreach ($month_array as $holiday_time => $holiday_array) {

              $row = array();
              $row[] = array('data' => storm_contrib_common_time_to_date(storm_contrib_common_timestamp_to_timezone($holiday_time), storm_contrib_common_get_custom_display_date_format()));

              $tmp = '';

              if (!empty($holiday_array)) {

                $cntr = 0;

                foreach ($holiday_array as $vid_person => $holiday_info) {

                  $cntr++;

                  if ($holiday_info['permitted'] == STORMPERSON_HOLIDAYS_STATUS_HOLIDAY_PERMITTED) {
                    $permitted = t('permitted');
                  }
                  elseif ($holiday_info['permitted'] == STORMPERSON_HOLIDAYS_STATUS_HOLIDAY_DECLINED) {
                    $permitted = t('declined');
                  }
                  else {
                    $permitted = t('pending permission');
                  }

                  if ($holiday_info['half_day']) {
                    $duration = t('half day');
                  }
                  else {
                    $duration = t('full day');
                  }


                  $tmp .= '<ul>';
                  $tmp .= '<li>';
                  $tmp .= l($holiday_info['person_title'], 'node/'. $holiday_info['nid'] .'/holidays');
                  $tmp .= ' - '. $duration;
                  $tmp .= ' ('. $permitted .')';
                  $tmp .= '</li>';
                  $tmp .= '</ul>';


                  // EXPORT
                  if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
                    if ($cntr > 1) {
                      $row[] = array('data' => storm_contrib_common_time_to_date(storm_contrib_common_timestamp_to_timezone($holiday_time), storm_contrib_common_get_custom_display_date_format()));
                    }
                    $row[] = array('data' => $tmp);
                    $rows[] = array('data' => $row);
                    $row = array();
                    $tmp = '';
                  }
                }
              }
            }
          }
        }

        if (!empty($rows)) {
          $content .= theme('table', $header, $rows);
        }

      }
      else {
        $content .= t('No items available');
      }

      $vars['filename'] = 'person-holidays';
      $vars['header'] = $header;
      $vars['rows'] = $rows;
      storm_exports_export($vars);
    }

    $content .= drupal_get_form('storm_exports_export_type_selection_form');
  }

  return $content;
}

function theme_stormperson_holidays_display_holidays_month($holidays, $start_time, $end_time, $mode, &$form = array()) {
  $content  = '';

  //Get the Month Name and Link it to the Year/Month
  $month = date('F', $start_time);
  if ($mode == 'month') {
    $month = l($month." ".date("Y", $start_time), "storm/holidays/display/".date("Y", $start_time));
  }
  elseif ($mode == 'year') {
    $month = l($month, "storm/holidays/display/".date("Y", $start_time)."-".date('m', $start_time));
  }
  else {
    $month = l($month, "storm/holidays/display/".date("Y", $start_time)."-".date('m', $start_time));
    if (date('F', $end_time) != $month) {
      $month .= " / ".l(date('F', $end_time), "storm/holidays/display/".date("Y", $end_time)."-".date('m', $end_time));
    }
    $month .= " ".l(date("Y", $start_time), "storm/holidays/display/".date("Y", $start_time));
  }

  //Prev&Next Links
  if ($mode != 'year') {
    if ($mode == "month") {
      $prev = date("Y", $start_time-86400)."-".date("m", $start_time-86400);
      $next = date("Y", $end_time+86400)."-".date("m", $end_time+86400);
    }elseif ($mode == "week") {
      $prev = date("Y", $start_time-86400)."-W".date("W", $start_time-86400);
      $next = date("Y", $end_time+86400)."-W".date("W", $end_time+86400);
    }
    $content .= '<div class="stormperson-holidays-heading-prev">'.l('<<', "storm/holidays/display/".$prev).'</div>';
    $content .= '<div class="stormperson-holidays-heading-next">'.l('>>', "storm/holidays/display/".$next).'</div>';
  }
  $content .= '<div class="stormperson-holidays-heading"><h3>'.$month.'</h3></div>';

  //Daynames - Table Header
  if ($mode == "month") {
    $header = array("", t('Mon'), t('Tue'), t('Wed'), t('Thu'), t('Fri'), t('Sat'), t('Sun'));
  }
  else {
    $header = array(t('Mon'), t('Tue'), t('Wed'), t('Thu'), t('Fri'), t('Sat'), t('Sun'));
  }

  $rows = array();

  $first_day = date('w', $start_time);
  $first_day = $first_day == 0 ? 7 : $first_day;

  $row = array();
  //add weeknumber to the first row/col
  if ($mode == "month") {
    $row[] = array('data' => l("W".date("W", $start_time), "storm/holidays/display/".date("Y", $start_time)."-W".date("W", $start_time)), 'class' => 'week-no');
  }
  //add cells, till we reached first day of the month
  for ($i=1;$i<$first_day;$i++) {
    $row[] = array('data' => '&nbsp;', 'class' => 'empty-date');
  }

  $holiday_rows = array(); $td_class = array();
  //cheack period for weekends and holidays, we add this as class for the cells
  $cnt = 0;
  for ($i=$start_time;$i<=$end_time;$i=$i+86400) {
    $cnt++;
    $gmtime = gmmktime(0,0,0,date('m', $i),date('d', $i),date('Y', $i));
    $td_class[$gmtime] = array();
    $td_class[$gmtime][$cnt] = 'normal';
    if (storm_holidays_check_if_day_is_weekend($i)) {
      $td_class[$gmtime][$cnt] = 'weekend';
    }
    $nathol = storm_holidays_check_if_day_is_a_holiday($i);
    if (!empty($nathol) && $nathol > 0) {
      $holiday_names[$gmtime] = storm_holidays_get_holiday_name($i);
    }
    if ($nathol == 1) {
      $td_class[$gmtime][$cnt] = 'national-holiday';
    }
    elseif($nathol == 0.5) {
      $td_class[$gmtime][$cnt] = 'national-half-holiday';
    }
    $td_class[$gmtime] = implode(' ', $td_class[$gmtime]);
  }
  unset($cnt);

  // TODO REVIEW - two time the same iteration with the same times !!

  //daylight saving time - i think i lost more hours, than i saved because of that... :P
  $last_dst = NULL;
  //iterate through the period
  for ($i=$start_time;$i<=$end_time;$i=$i+86400) {
    storm_contrib_common_check_4_dst($i, $last_dst);
    //holidays are saved in gmtime - and i work with it, because no DST
    $gmtime = gmmktime(0,0,0,date('m', $i),date('d', $i),date('Y', $i));

    //add the date cell
    if (!empty($td_class[$gmtime])) {
      if (!empty($holiday_names[$gmtime])) {
        $row[] = array('data' => date('j', $i), 'class' => 'date '.$td_class[$gmtime], 'title' => $holiday_names[$gmtime]);
      }
      else {
        $row[] = array('data' => date('j', $i), 'class' => 'date '.$td_class[$gmtime]);
      }

    }

    $month = date('F', $i);
    $day_of_week = date('w', $i);;
    $day_of_week = $day_of_week == 0 ? 7 : $day_of_week;

    //have anyone holidays?
    if (!empty($holidays[$month]) && is_array($holidays[$month]) && !empty($holidays[$month][$gmtime])) {

      //iterate through the persons who have holiday at this day
      foreach ($holidays[$month][$gmtime] as $vid=>$holiday_info) {
        $holiday_rows[$vid] = array();
        //add blank cells, to this date
        if ($day_of_week > 1) {
          for($k=1;$k<$day_of_week;$k++) {
            $holiday_rows[$vid][] = array('data' => '&nbsp', 'class' => $td_class[$gmtime-86400*($day_of_week-$k)]);
          }
        }

        //this is for the edit form page, where we not only want to display all holidays, instead we add form item for the edit user here
        if (!empty($form['data']['#value']['edit_user']) && $form['data']['#value']['edit_user']->vid == $vid) {
          for($k=$gmtime;$k<=((7-$day_of_week)*86400+$gmtime);$k=$k+86400) {
            $month = date('F', $k);
            if (!isset($td_class[$k])) {
              $td_class[$k] = 'empty-date';
            }
            if (!empty($holidays[$month][$k][$vid])) {
              $add_class = array();
              if ($holidays[$month][$k][$vid]['permitted'] == STORMPERSON_HOLIDAYS_STATUS_HOLIDAY_PERMITTED) {
                $permitted = t('permitted');
                $add_class[] = 'holiday-permitted';
              }
              elseif ($holidays[$month][$k][$vid]['permitted'] == STORMPERSON_HOLIDAYS_STATUS_HOLIDAY_DECLINED) {
                $permitted = t('declined');
                $add_class[] = 'holiday-declined';
              }
              else {
                $permitted = t('pending permission');
                $add_class[] = 'holiday-pending';
              }
              if ($holidays[$month][$k][$vid]['half_day']) {
                $duration = " (".t('half day').") ";
                $add_class[] = 'holiday-half';
              }
              else {
                $duration = "";
              }
              $class = implode(" ", $add_class);
              $row_content = '';
              if (!empty($form['select_allocated_holidays_cal'][$k])) {
                $form['select_allocated_holidays_cal'][$k]['#title'] = t('Edit holiday').$duration.' ['.$permitted.']';
                $row_content .= drupal_render($form['select_allocated_holidays_cal'][$k]);
              }
              if (empty($row_content)) {
                $row_content = l($holiday_info['person_title'], 'node/'. $holiday_info['nid'] .'/holidays').$duration.' ('.$permitted.')';
              }
              $holiday_rows[$vid][] = array('data' => $row_content, 'class' => 'date-holiday '.$class);
            }
            else {
              $holiday_rows[$vid][] = array('data' => '&nbsp', 'class' => $td_class[$k]);
            }
            unset($holidays[$month][$k][$vid]);
          }
        }
        //and here is the logic, for only displaying the holidays
        else {
          //from here, we check the whole week till the end - to determine the colspan
          $number_holidays = 0;
          for($k=$gmtime;$k<=((7-$day_of_week)*86400+$gmtime);$k=$k+86400) {
            if (!isset($td_class[$k])) {
              $td_class[$k] = 'empty-date';
            }
            $month = date('F', $k);
            //the person have more than one day holiday, so continue
            if (is_array($holidays[$month][$k]) && !empty($holidays[$month][$k][$vid]) &&
              $holiday_info['permitted'] == $holidays[$month][$k][$vid]['permitted'] && $holiday_info['half_day'] == $holidays[$month][$k][$vid]['half_day']
            ) {
              $number_holidays++;

            }
            //the holiday ended or the state changed
            else {
              //print the holiday
              if (!empty($holiday_info)) {
                $add_class = array();
                if ($holiday_info['permitted'] == STORMPERSON_HOLIDAYS_STATUS_HOLIDAY_PERMITTED) {
                  $permitted = t('permitted');
                  $add_class[] = 'holiday-permitted';
                }
                elseif ($holiday_info['permitted'] == STORMPERSON_HOLIDAYS_STATUS_HOLIDAY_DECLINED) {
                  $permitted = t('declined');
                  $add_class[] = 'holiday-declined';
                }
                else {
                  $permitted = t('pending permission');
                  $add_class[] = 'holiday-pending';
                }

                if ($holiday_info['half_day']) {
                  $duration = " - ".t('half day');
                  $add_class[] = 'holiday-half';
                }
                else {
                  $duration = "";
                }

                $class = implode(" ", $add_class);

                $row_content = l($holiday_info['person_title'], 'node/'. $holiday_info['nid'] .'/holidays').$duration.' ('.$permitted.')';
                $holiday_rows[$vid][] = array('data' => $row_content, 'class' => 'date-holiday '.$class, 'colspan' => $number_holidays);

                $holiday_info = array();
                $number_holidays = 0;
              }
              //state of holiday changed, start all over with new status
              if (is_array($holidays[$month][$k]) && !empty($holidays[$month][$k][$vid])) {
                $number_holidays++;
                $holiday_info = $holidays[$month][$k][$vid];
              }
              //print empty cells - working day or weekends
              else {
                $holiday_rows[$vid][] = array('data' => '&nbsp', 'class' => $td_class[$k]);
              }
            }
            unset($holidays[$month][$k][$vid]);
          }
        }
      }
    }

    //a week is over, now we can add the dates and the holidays to the table
    if (date('w', $i) == 0) {
      if ($mode == "month" && count($holiday_rows) > 0) {
        $row[0]['rowspan'] = count($holiday_rows)+1;
      }
      $tr_class = (date('W', $i) % 2 == 0) ? 'odd' : 'even';
      $tr_class = "week-".$tr_class;
      $rows[] = array('data' => $row, 'class' => 'date '.$tr_class);
      foreach($holiday_rows as $hrow) {
        $rows[] = array('data' => $hrow, 'class' => $tr_class);
      }
      $row = array();
      $holiday_rows = array();
      if ($mode == "month") {
        $row[] = array('data' => l("W".date("W", $i+86400), "storm/holidays/display/".date("Y", $i+86400)."-W".date("W", $i+86400)), 'class' => 'week-no');
      }
    }
  }

  //last row
  if (!empty($row)) {
    if ($mode == "month" && count($holiday_rows) > 0) {
      $row[0]['rowspan'] = count($holiday_rows)+1;
    }
    for ($i=date('w', $end_time);$i<7;$i++) {
      $row[] = array('data' => '&nbsp;', 'class' => 'empty-date');
    }
    $tr_class = (date('W', $end_time) % 2 == 0) ? 'odd' : 'even';
    $tr_class = "week-".$tr_class;
    $rows[] = array('data' => $row, 'class' => 'date '.$tr_class);
    foreach($holiday_rows as $hrow) {
      $rows[] = array('data' => $hrow, 'class' => $tr_class);
    }
  }

  $content .= theme('table', $header, $rows, array('class' => 'stormperson-holidays-calendar-month'));

  return $content;
}

function theme_stormperson_holidays_display_holidays_form($form) {
  $data = $form['data']['#value'];

  drupal_add_css(drupal_get_path('module', 'stormperson_holidays')."/stormperson_holidays.css");

  $content  = '';

  if (!empty($form['department_selection'])) {
    $content .= drupal_render($form['department_selection']);
  }

  $year = date("Y", $data['start_time']);
  for ($month=1; $month<=12; $month++) {
    $content .= '<div class="stormperson-holidays-calendar-year-month month-'.$month.'">';
    $start_time_month = mktime(0,0,0,$month, 1, $year);
    $end_time_month = mktime(0,0,0,$month, date("t", $start_time_month), $year);
    $content .= theme_stormperson_holidays_display_holidays_month($data['holidays'], $start_time_month, $end_time_month, 'year', $form);
    $content .= '</div>';
  }
  if (!empty($form['select_allocated_holidays_cal'])) {
    $form['select_allocated_holidays_cal']['#title'] = "";
    $content .= drupal_render($form['select_allocated_holidays_cal']);
  }

  unset($form['data']);
  $content .= drupal_render($form);


  return $content;
}

function theme_stormperson_holidays_block_request($result) {
  $content = "";

  $rows = array();
  foreach ($result['holidays'] as $person_holiday) {
    $row = array();
    $row[] = array('data' => storm_contrib_common_time_to_date($person_holiday['request_date'], storm_contrib_common_get_custom_display_date_format()));
    $row[] = array('data' => l($person_holiday['author_title'], 'node/'.$person_holiday['author_nid']));
    $row[] = array('data' => storm_contrib_common_time_to_date($person_holiday['holiday_begin'], storm_contrib_common_get_custom_display_date_format()));
    $row[] = array('data' => storm_contrib_common_time_to_date($person_holiday['holiday_end'], storm_contrib_common_get_custom_display_date_format()));
    $row[] = array('data' => $person_holiday['holiday_count']);

    $row[] = array('data' => $person_holiday['remaining']);

    $hint = "";
    if ($person_holiday['holiday_count_half'] > 0) {
      $hint .= '<span class="half-holiday">'.format_plural($person_holiday['holiday_count_half'], '@count half Holiday', '@count half Holidays').'</span>';
    }
    if ($person_holiday['planed_tasks_tickets']['count'] > 0) {
      if (!empty($hint)) {
        $hint .= " <br/>";
      }
      $hint .= '<span class="planed-tasks">'.format_plural($person_holiday['planed_tasks_tickets']['count'], '@count planed Task/Ticket', '@count planed Tasks/Tickets').'</span>';
    }

    $row[] = array('data' => $hint);

    $row[] = array('data' => l(t('Details'), 'node/'.$person_holiday['author_nid'].'/holidays'));

    $rows[] = array('data' => $row);
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => 8));
  }

  $content .= theme('table', $result['header'], $rows);

  $content .= $result['pager'];

  return $content;
}