<?php

/**
 * theme the block, where we show all the filters, the user can use
 * @param array $filters
 * @return string
 */
function theme_storm_contrib_filter_block_overview($filters = array()) {
  drupal_add_css(drupal_get_path('module', 'storm_contrib_filter')."/storm_contrib_filter.css");
  $output = "";

  $output .= '<ul class="storm-contrib-filter-block">';

  foreach ($filters as $filter) {
    global $user;
    // we add a specific class to the li, so we can change the look of default an public filter
    $class = array('storm-contrib-filter');
    if ($filter['filter_public'] == 1 && $filter['filter_uid'] != $user->uid) {
      $class[] = 'filter_public';
    }
    elseif ($filter['filter_public'] == 1 && $filter['filter_uid'] == $user->uid) {
      $class[] = 'filter_public_own';
    }
    if ($filter['filter_default'] == 1) {
      $class[] = 'filter_default';
    }
    $class = implode(' ', $class);
    $from = ($filter['filter_uid'] != $user->uid) ? t(' by !username', array('!username' => theme('username', user_load($filter['filter_uid'])))) : "";
    $link_class = '';
    if (!empty($GLOBALS['storm_contrib_filter']) && $GLOBALS['storm_contrib_filter']['filter_id'] == $filter['filter_id']) {
      $link_class = 'active';
    }

    // when using l here, the link is active by default, but we want it only active, if the current filter is loaded
    $output .= '<li class="'.$class.'"><a class="'.$link_class.'" href="'.check_url(url($_GET['q'], array('query' => 'filter_load='.$filter['filter_id']))).'">'.$filter['filter_name'].'</a>'.$from.'</li>';
  }

  $output .= '</ul>';

  return $output;
}