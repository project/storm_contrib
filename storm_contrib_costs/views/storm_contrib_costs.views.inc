<?php

/**
 * @file storm_contrib_costs.views.inc
 */

/**
 * Implementation of hook_views_data()
 * 
 * @return unknown_type
 */
function storm_contrib_costs_views_data() {
  
  // ---------------------------------------
  // TABLE STORM CONTRIB COSTS PERSON COSTS
  // ---------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['storm_contrib_costs_person_costs']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['storm_contrib_costs_person_costs']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // numeric text field person costs
  $data['storm_contrib_costs_person_costs']['costs'] = array(
    'title' => t('Costs - Person Costs'),
    'help' => t('The entered costs of a person per unit.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // plain text field person costs unit
  $data['storm_contrib_costs_person_costs']['unit'] = array(
    'title' => t('Costs - Person Costs Unit'),
    'help' => t('The selected unit of the person costs (hour/day,...).'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // plain text field person costs currency
  $data['storm_contrib_costs_person_costs']['currency'] = array(
    'title' => t('Costs - Person Costs Currency'),
    'help' => t('The selected currency of the person costs per unit (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  
  // -------------------------------------------
  // TABLE STORM CONTRIB COSTS ESTIMATED COSTS
  // -------------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['storm_contrib_costs_estimated_costs']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['storm_contrib_costs_estimated_costs']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // numeric text field estimated costs
  $data['storm_contrib_costs_estimated_costs']['estimated_costs'] = array(
    'title' => t('Costs - Estimated Costs'),
    'help' => t('The total estimated costs of a project, task or ticket, calculated by the system.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // plain text field estimated costs currency
  $data['storm_contrib_costs_estimated_costs']['estimated_costs_currency'] = array(
    'title' => t('Costs - Estimated Costs Currency'),
    'help' => t('The currency of the total estimated costs of a project, task or ticket (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  // numeric text field estimated timetracking costs
  $data['storm_contrib_costs_estimated_costs']['estimated_timetracking_costs'] = array(
    'title' => t('Costs - Estimated Timetracking Costs'),
    'help' => t('The estimated costs just calculated by durations and timetrackings of a project, task or ticket, calculated by the system.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // plain text field estimated timetracking costs currency
  $data['storm_contrib_costs_estimated_costs']['estimated_timetracking_costs_currency'] = array(
    'title' => t('Costs - Estimated Timetracking Costs Currency'),
    'help' => t('The currency of the estimated costs just calculated by durations and timetrackings of a project, task or ticket, (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // numeric text field estimated duration in hours
  $data['storm_contrib_costs_estimated_costs']['estimated_duration_in_hours'] = array(
    'title' => t('Costs - Estimated Duration In Hours'),
    'help' => t('The estimated duration in hours of a project, task or ticket, calculated by the system by the entered durations.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // timestamp field last updated
  $data['storm_contrib_costs_estimated_costs']['last_updated'] = array(
    'title' => t('Costs - Estimated Costs/Durations Last Updated'),
    'help' => t('The date when the estimated costs/durations calculation was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  
  // -------------------------------------------
  // TABLE STORM CONTRIB COSTS TOTAL COSTS
  // -------------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['storm_contrib_costs_total_costs']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['storm_contrib_costs_total_costs']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // numeric text field total costs
  $data['storm_contrib_costs_total_costs']['total_costs'] = array(
    'title' => t('Costs - Total Costs'),
    'help' => t('The total costs of a project, task or ticket, calculated by the system by timetrackings and expenses.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // plain text field total costs currency
  $data['storm_contrib_costs_total_costs']['total_costs_currency'] = array(
    'title' => t('Costs - Total Costs Currency'),
    'help' => t('The currency of the total costs of a project, task or ticket (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  // numeric text field total timetrackings costs
  $data['storm_contrib_costs_total_costs']['total_timetracking_costs'] = array(
    'title' => t('Costs - Total Timetracking Costs'),
    'help' => t('The total costs of a project, task or ticket, calculated by the system just considering timetrackings.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // plain text field total costs currency
  $data['storm_contrib_costs_total_costs']['total_timetracking_costs_currency'] = array(
    'title' => t('Costs - Total Timetracking Costs Currency'),
    'help' => t('The currency of the total timetracking costs of a project, task or ticket (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  // numeric text field total duration in hours
  $data['storm_contrib_costs_total_costs']['total_duration_in_hours'] = array(
    'title' => t('Costs - Total Time Taken In Hours'),
    'help' => t('The total duration in hours taken till now of a project, task or ticket, calculated by the system considering the timetrackings.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // numeric text field total billing duration in hours
  $data['storm_contrib_costs_total_costs']['total_billing_duration_in_hours'] = array(
    'title' => t('Costs - Total Billing Time Taken In Hours'),
    'help' => t('The total billing duration in hours taken till now of a project, task or ticket, calculated by the system considering the timetrackings.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // timestamp field last updated
  $data['storm_contrib_costs_total_costs']['last_updated'] = array(
    'title' => t('Costs - Total Costs/Durations Last Updated'),
    'help' => t('The date when the total costs/durations calculation was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  
  
  // --------------------------------------------------
  // TABLE STORM CONTRIB COSTS CURRENCY NAMES
  // --------------------------------------------------

  $data['storm_contrib_costs_currency_names']['table']['base'] = array(
  'field' => 'currency_code',
  'title' => t('Costs - Currency Full Names'),
  'help' => t("The full currency names to each currency rate code."),
  'weight' => -10,
  );
  
  $data['storm_contrib_costs_currency_names']['table']['group']  = 'Storm';
  
  // plain text field currency code
  $data['storm_contrib_costs_currency_names']['currency_code'] = array(
    'title' => t('Costs - Currency Code'),
    'help' => t('The currency code (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // plain text field currency name
  $data['storm_contrib_costs_currency_names']['currency_name'] = array(
    'title' => t('Costs - Currency Full Name'),
    'help' => t('The full name of a currency code, exp.: "US dollar" for code "USD".'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  
  // --------------------------------------------------
  // TABLE STORM CONTRIB COSTS CURRENCY EXCHANGE RATES
  // --------------------------------------------------
  
  
   $data['storm_contrib_costs_currency_exchange_rates']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['storm_contrib_costs_currency_exchange_rates']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'storm_contrib_costs_currency_names' => array(
      'left_field' => 'currency_code',
      'field' => 'currency_code',
    ),
  );
  
  
  // plain text field currency code
  $data['storm_contrib_costs_currency_exchange_rates']['currency_code'] = array(
    'title' => t('Costs - Currency Exchange Rate Code'),
    'help' => t('The currency exchange rate code (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // numeric text field currency exchange rate
  $data['storm_contrib_costs_currency_exchange_rates']['exchange_rate'] = array(
    'title' => t('Costs - Currency Exchange Rate'),
    'help' => t('The currency exchange rate always referenced to one Euro.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // timestamp field last updated
  $data['storm_contrib_costs_currency_exchange_rates']['last_updated'] = array(
    'title' => t('Costs - Currency Exchange Rate Last Updated'),
    'help' => t('The date when the currency exchange rate were last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  
  
  return $data;
}

