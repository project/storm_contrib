<?php

/*
 * @file stormtask_extension.theme.inc
 */

/**
 * theme project page view add ons
 *
 * @param unknown_type $total_duration
 * @param unknown_type $latest_end_date
 * @return unknown_type
 */
function theme_stormtask_extension_page_view_adds($node) {
  $content = '';
  $return = array();

  drupal_add_css(drupal_get_path('module', 'stormtask_extension') .'/stormtask_extension.css');

  //  ASSIGNED DEPARTMENT
  if (!empty($node->department_terms)) {
    $departments = array();
    foreach ($node->department_terms as $department_term) {
      $departments[] = $department_term->name;
    }
    if (!empty($departments)) {
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Department'), '<span class="stormtask-department">'. implode(', ', $departments) .'</span>');
      $content .= '</div>';
    }
  }

  // --------
  // AUTHOR
  // --------
  $content .= '<div class="stormfields">';
  if (!empty($node->author_nid)) {
    $content .= theme('storm_view_item', t('Author'), '<span class="stormticket-author">'. l($node->author_name, 'node/'. $node->author_nid) .'</span>');
  }
  else {
    $content .= theme('storm_view_item', t('Author'), '<span class="stormticket-author">'. l($node->author_name, 'user/'. $node->uid) .'</span>');
  }
  $content .= '</div>';

  // --------
  // CREATED
  // --------
  $content .= '<div class="stormfields">';
  $content .= theme('storm_view_item', t('Created'), '<span class="stormtask-createdr">'. storm_contrib_common_time_to_date($node->created, storm_contrib_common_get_custom_display_date_format() .' - H:i:s') .'</span>');
  $content .= '</div>';

  $return['author'] = array(
    '#value' => $content,
    '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'stormticket_extension_add_ons_author') : -13,
  );
  $content = '';

  // ------------
  // DURATIONS
  // ------------

  $content .= '<div id="stormtask-extension-page-view-adds-durations">';

  $class = 'balance-ok';

  // DATE BEGIN
  if (!empty($node->datebegin)) {
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Date begin'), '<span class="'. $class .'">'. storm_contrib_common_time_to_date($node->datebegin, storm_contrib_common_get_custom_display_date_format()) .'</span>');
    $content .= '</div>';
  }

  // DATE END
  if (!empty($node->dateend)) {
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Date end'), '<span class="'. $class .'">'. storm_contrib_common_time_to_date($node->dateend, storm_contrib_common_get_custom_display_date_format()) .'</span>');
    $content .= '</div>';
  }

  // LATEST ASSIGNED END DATE OF TICKETS
  if (!empty($node->latest_end_date['end_date'])) {
    if ($node->latest_end_date['end_date'] > $node->dateend && !empty($node->dateend)) {
      $class = 'balance-error';
    }
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Latest assigned end date'), '<span class="'. $class .'">'. storm_contrib_common_time_to_date($node->latest_end_date['end_date'], storm_contrib_common_get_custom_display_date_format()) .' '. l(t('to task/ticket'), 'node/'. $node->latest_end_date['nid']) .'</span>');
    $content .= '</div>';
    $class = 'balance-ok';
  }


  // ASSIGNED DURATION
  $recurring_duration = '';
  if ($node->is_recurring_duration) {
    $recurring_duration = ' - '. ('recurring duration');
  }
  $content .= '<div class="stormfields">';
  $content .= theme('storm_view_item', t('Entered task duration'), '<span class="'. $class .'">'. format_plural(theme('storm_contrib_common_number_format', $node->duration), '@count @unit', '@count @unit', array('@unit' => $node->durationunit) ) . $recurring_duration .'</span>');
  $content .= '</div>';



  // IF USER IS ALLOWED TO SEE DURATIONS AND COSTS
  // (VALUES ARE EMPTY IF USER HAS NOT THE PERMISSION)
//  if ($node->access_costs) {

    // ESTIMATED DURATION
    if (!empty($node->estimated_duration)) {
      if (!empty($node->duration) && $node->estimated_duration > $node->duration) {
        $class = 'balance-error';
      }
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Estimated duration'), '<span class="'. $class .'">'. format_plural(theme('storm_contrib_common_number_format', $node->estimated_duration), '@count @unit', '@count @unit', array('@unit' => $node->durationunit)) .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->estimated_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
      $class = 'balance-ok';
    }

    // STILL OPEN DURATION CALCULATED BY SYSTEM
    if (!empty($node->total_open_duration)) {
      if (!empty($node->duration) && $node->total_open_duration > $node->duration ) {
        $class = 'balance-error';
      }
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Still open durations'), '<span class="'. $class .'">'. format_plural(theme('storm_contrib_common_number_format', $node->total_open_duration), '@count @unit', '@count @unit', array('@unit' => $node->durationunit)) .'</span> <span class="last-update">('. t('real time calculation') .')</span>');
      $content .= '</div>';
    }
    $class = 'balance-ok';

    // REAL DURATION TILL NOW
    if (!empty($node->total_real_duration)) {
      if (!empty($node->duration) && $node->total_real_duration > $node->duration ) {
        $class = 'balance-error';
      }
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Time taken'), '<span class="'. $class .'">'. format_plural(theme('storm_contrib_common_number_format', $node->total_real_duration), '@count @unit', '@count @unit', array('@unit' => $node->durationunit)) .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->total_real_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
    }
    $class = 'balance-ok';

    // STILL OPEN DURATION + REAL DURATION TILL NOW
    if (!empty($node->still_open_total_real_duration)) {
      if ($node->still_open_total_real_duration > $node->duration && !empty($node->duration)) {
        $class = 'balance-error';
      }
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Open durations + time taken'), '<span class="'. $class .'">'. format_plural(theme('storm_contrib_common_number_format', $node->still_open_total_real_duration), '@count @unit', '@count @unit', array('@unit' => $node->durationunit)) .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->total_real_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
      $class = 'balance-ok';
    }

//  }

  $content .= '</div>';

  $return['timing'] = array(
    '#value' => $content,
    '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'stormticket_extension_add_ons_timing') : -12.8,
  );
  $content = '';

  // IF USER IS ALLOWED TO SEE DURATIONS AND COSTS
  // (VALUES ARE EMPTY IF USER HAS NOT THE PERMISSION)
  if ($node->access_costs) {

    // -------
    // COSTS
    // -------

    $content .= '<div id="stormtask-extension-page-view-adds-costs">';
    $empty = true;

    // ESTIMATED COSTS
    if (!empty($node->estimated_costs)) {
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Estimated costs'), '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $node->estimated_costs) .' '. $node->estimated_costs_currency .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->estimated_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
      $empty = false;
    }

    // ESTIMATED TIMETRACKING COSTS
    if (!empty($node->estimated_timetracking_costs)) {
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Estimated timetracking costs'), '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $node->estimated_timetracking_costs) .' '. $node->estimated_timetracking_costs_currency .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->estimated_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
      $empty = false;
    }

    // REAL TIMETRACKING COSTS TILL NOW
    if (!empty($node->total_real_timetracking_costs)) {
      if ($node->total_real_timetracking_costs > $node->estimated_costs && !empty($node->estimated_costs)) {
        $class = 'balance-error';
      }
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Timetracking costs taken'), '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $node->total_real_timetracking_costs) .' '. $node->total_real_timetracking_costs_currency .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->total_real_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
      $class = 'balance-ok';
      $empty = false;
    }

    // REAL COSTS TILL NOW
    if (!empty($node->total_real_costs)) {
      if ($node->total_real_costs > $node->estimated_costs && !empty($node->estimated_costs)) {
        $class = 'balance-error';
      }
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Real costs taken'), '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $node->total_real_costs) .' '. $node->total_real_costs_currency .'</span> <span class="last-update">('. storm_contrib_common_time_to_date($node->total_real_costs_last_updated, storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>');
      $content .= '</div>';
      $class = 'balance-ok';
      $empty = false;
    }

    $content .= '</div>';

    if (!$empty) {
      $return['costs'] = array(
        '#value' => $content,
        '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'stormticket_extension_add_ons_costs') : -12.6,
      );
    }

  }

  return $return;
}

/**
 * timetracking button
 *
 * @param unknown_type $node
 * @return unknown_type
 */
function theme_stormtask_extension_task_page_view_timetracking_trigger($node) {

  $content = '';

  // QUICK TIMETRACKING
  if (module_exists('storm_dashboard')) {
    $content .= _storm_dashboard_timetracking_trigger(TRUE, $node->nid) .' '. t('Click on clock to start timetracking');
  }
  return $content;
}