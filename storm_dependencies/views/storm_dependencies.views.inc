<?php

/**
 * @file storm_dependencies.views.inc
 */

/**
 * Implementation of hook_views_data()
 * 
 * @return unknown_type
 */
function storm_dependencies_views_data() {
  
  // --------------------------------------
  // TABLE STORM DEPENDENCIES
  // --------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['storm_dependencies']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['storm_dependencies']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // numeric text field budget costs
  $data['storm_dependencies']['parent_nid'] = array(
    'title' => t('Dependencies - Parent Nid'),
    'help' => t('The parent nid of a ticket or task.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  
  return $data;
}

