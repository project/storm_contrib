<?php

/**
 * @file storm_dependencies.module
 */

/**
 * node types a ticket could depend on
 *
 * @return unknown_type
 */
function storm_dependencies_dependable_node_types() {
  return array(
  'stormticket' => t('Ticket'),
  'stormtask' => t('Task'),
  );
}

/**
 * admin form settings add ons
 *
 * @param unknown_type $form
 * @return unknown_type
 */
function storm_dependencies_storm_contrib_common_admin_settings_form($form) {

  // ---------------------------
  // DEPENDENCIES SETTINGS
  // ---------------------------
  $form['storm_dependencies'] = array(
  '#type' => 'fieldset',
  '#title' => t('Storm Dependencies Settings'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  '#weight' => 20,
  );

  $form['storm_dependencies']['storm_dependencies_avoid_deleting_of_node_if_dependencies_old'] = array(
  '#type' => 'value',
  '#value' => variable_get('storm_dependencies_avoid_deleting_of_node_if_dependencies', ''),
  );

  $form['storm_dependencies']['storm_dependencies_avoid_deleting_of_node_if_dependencies'] = array(
  '#type' => 'checkbox',
  '#title' => t('Avoid deleting of nodes if node has dependencies or assignments'),
  '#description' => t('If set active the node will not be deleted if it still has dependencies or assigned nodes like timetrackings, projects, tasks or tickets. - Clear menu cache after changing this setting !'),
  '#default_value' => variable_get('storm_dependencies_avoid_deleting_of_node_if_dependencies', ''),
  );

  $form['#submit'][] = 'storm_dependencies_storm_contrib_common_admin_settings_form_submit';

  return $form;
}


/**
 * submit of the storm_contrib admin settings
 * @param $form
 * @param $form_state
 */
function storm_dependencies_storm_contrib_common_admin_settings_form_submit($form, $form_state) {

  if ($form_state['values']['storm_dependencies_avoid_deleting_of_node_if_dependencies'] != $form_state['values']['storm_dependencies_avoid_deleting_of_node_if_dependencies_old']) {
    drupal_set_message(t('You have to clear menu cache to get the new settings!'), 'error');
  }

}


/**
 * check if deleting of nodes should be avoided if node has dependencies or assignments
 *
 * @return unknown_type
 */
function storm_dependencies_avoid_deleting_of_node_if_dependencies() {
  static $storm_dependencies_avoid_deleting_of_node_if_dependencies;
  if (empty($storm_dependencies_avoid_deleting_of_node_if_dependencies)) {
    $storm_dependencies_avoid_deleting_of_node_if_dependencies = variable_get('storm_dependencies_avoid_deleting_of_node_if_dependencies', '');
  }
  return $storm_dependencies_avoid_deleting_of_node_if_dependencies;
}


/**
 * Implementation of hook_menu()
 * @return unknown_type
 */
function storm_dependencies_menu() {

  $items = array();

  $items['storm/ticket/select-parent-node'] = array(
    'title' => 'Select parent node',
    'page callback' => 'storm_dependencies_autocomplete_select_parent_node',
    'access arguments' => array('Storm ticket: add'),
    'type' => MENU_CALLBACK,
  );

  $items['node/%node/dependencies'] = array(
    'title' => 'Dependencies',
    'page callback' => 'storm_dependencies_display_node_dependencies_and_assignments',
    'page arguments' => array(1),
    'access arguments' => array('Storm ticket: add'), // TODO CHANGE PERMISSION
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implementation of hook_menu()
 * @return unknown_type
 */
function storm_dependencies_menu_alter(&$items) {

  if (!empty($items)) {

    // IF SET IN ADMIN SETTINGS TO AVOID NODE DELETING IF NODE HAS DEPENDENCIES OR ASSIGNMENTS
    if (storm_dependencies_avoid_deleting_of_node_if_dependencies()) {

      // AVOID DELETING OF NODES WITH DEPENDENCIES
      if (!empty($items['node/%node/delete'])) {
        $items['node/%node/delete']['page callback'] = 'storm_dependencies_node_delete_confirm';
        $items['node/%node/delete']['page arguments'] = array(1);
      }

    }

  }

}


/**
 * Implementation of hook_views_api()
 *
 * @return unknown_type
 */
function storm_dependencies_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'storm_dependencies') . '/views',
  );
}


/**
 * Implementation of hook_theme()
 * @return unknown_type
 */
function storm_dependencies_theme($existing, $type, $theme, $path) {
  return array(
    'storm_dependencies_display_parent_node' => array(
      'arguments' => array('node' => NULL),
      'file' => 'storm_dependencies.theme.inc',
    ),
    'storm_dependencies_display_node_dependencies_and_assignments' => array(
      'arguments' => array('node' => NULL, 'dependencies' => array()),
      'file' => 'storm_dependencies.theme.inc',
    ),
  );
}


/**
 * Implementation of hook_form_alter()
 */
function storm_dependencies_form_alter(&$form, $form_state, $form_id) {

  switch ($form_id) {

    case 'stormticket_node_form':
    case 'stormtask_node_form':

      drupal_add_css(drupal_get_path('module', 'storm_dependencies') .'/storm_dependencies.css');

      $_SESSION['storm_dependencies_autocomplete_project_nid'] = 0;
      $_SESSION['storm_dependencies_autocomplete_task_nid'] = 0;

      if (!empty($_GET['project_nid'])) {
        $_SESSION['storm_dependencies_autocomplete_project_nid'] = check_plain($_GET['project_nid']);
      }
      if (!empty($_GET['task_nid'])) {
        $_SESSION['storm_dependencies_autocomplete_task_nid'] = check_plain($_GET['task_nid']);
        // GET PROJECT NID
        if (!empty($_SESSION['storm_dependencies_autocomplete_task_nid']) && is_numeric($_SESSION['storm_dependencies_autocomplete_task_nid'])) {
          $task_node = node_load($_SESSION['storm_dependencies_autocomplete_task_nid']);
          if (!empty($task_node->project_nid)) {
            $_SESSION['storm_dependencies_autocomplete_project_nid'] = $task_node->project_nid;
          }
          unset($task_node);
        }
      }



      $node = $form['#node'];

      // AUTOCOMPLETE FIELD TO SELECT A PARENT TICKET OR TASK
      $default = '';
      if (!empty($node->parent_stormnode)) {
        $default = $node->parent_stormnode;
      }
      $form['parent_stormnode'] = array(
      '#type' => 'textfield',
      '#title' => t('Ticket/Task depends on'),
      '#description' => t('Select a ticket or task on which this ticket depends on'),
      '#autocomplete_path' => 'storm/ticket/select-parent-node',
      '#weight' => -19.5,
      '#default_value' => $default,
      );

      if (!empty($node->parent_stormnode_nid)) {
        $form['parent_stormnode_nid'] = array(
        '#type' => 'value',
        '#value' => $node->parent_stormnode_nid,
        );
      }

      $form['#validate'][] = 'storm_dependencies_node_form_validate';
      $form['#submit'][] = 'storm_dependencies_node_form_submit';

      break;

  }

}



/**
 * validate the stormticket node form
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function storm_dependencies_node_form_validate($form, &$form_state) {

  if (t('Save') == $form_state['values']['op']) {

    if (!empty($form_state['values']['parent_stormnode'])) {

      $node = $form['#node'];

      // ---------------------------------------------------------------------
      // CHECK IF THE ASSIGNED TICKET IS ALREADY ALSO ASSIGNED TO THIS TICKET
      // ---------------------------------------------------------------------

      $parent_nid = $node->parent_stormnode_nid;

      if (!empty($parent_nid) && is_numeric($parent_nid)) {
        $parent_node = node_load($parent_nid);
        if ($parent_node->parent_stormnode_nid == $node->nid) {
          // THE PARENT TICKET HAS ALREADY THIS TICKET ASSIGNED
          form_set_error('parent_stormnode', t('The selected parent ticket $parent_title has already this ticket assigned as parent ticket', array('$parent_title' => $parent_node->title)));
        }
      }

    }

  }
}

/**
 * submit of the stormticket node form
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function storm_dependencies_node_form_submit($form, &$form_state) {
}

/**
 * autocomplete function to select a parent ticket
 * @return unknown_type
 */
function storm_dependencies_autocomplete_select_parent_node($string = '') {

  $matches = array();
  $args = array();
  $project_nid = 0;
  $task_nid = 0;

  $dependable_node_types = storm_dependencies_dependable_node_types();

  // GET NODES TO REFERENCE
  $sql  = 'SELECT n.nid, n.title, n.type, ';
  $sql .= 'sti.project_nid AS ticket_project_nid, sti.project_title AS ticket_project_title, ';
  $sql .= 'sta.project_nid AS task_project_nid, sta.project_title AS task_project_title ';
  $sql .= 'FROM {node} n ';
  $sql .= 'LEFT JOIN {stormticket} sti ON n.vid = sti.vid ';
  $sql .= 'LEFT JOIN {stormtask} sta ON n.vid = sta.vid ';
  $sql .= 'WHERE n.type IN ('. db_placeholders($dependable_node_types, 'varchar') .') ';
  $args = array_keys($dependable_node_types);
  $sql .= 'AND n.title LIKE \'%%%s%%\' ';
  $args[] = $string;
  $open_status_ticket = storm_contrib_common_storm_item_status_open('ticket');
  $open_status_task = storm_contrib_common_storm_item_status_open('task');
  $sql .= 'AND (sta.taskstatus IN ('. db_placeholders($open_status_task, 'varchar') .') OR sti.ticketstatus IN ('. db_placeholders($open_status_ticket, 'varchar') .')) ';
  $args = array_merge($args, $open_status_task);
  $args = array_merge($args, $open_status_ticket);

  // IF PROJECT NID IS SET
  if (!empty($_SESSION['storm_dependencies_autocomplete_project_nid']) && is_numeric($_SESSION['storm_dependencies_autocomplete_project_nid'])) {
    $sql .= 'AND (sti.project_nid = %d ';
    $args[] = $_SESSION['storm_dependencies_autocomplete_project_nid'];
    $sql .= 'OR sta.project_nid = %d) ';
    $args[] = $_SESSION['storm_dependencies_autocomplete_project_nid'];
  }

  $result = db_query(db_rewrite_sql($sql, 'n', 'nid'), $args);
  if (!empty($result)) {
    while ($row = db_fetch_array($result)) {

      $project_title = '';

      switch ($row['type']) {

        case 'stormtask':
          $project_title = $row['task_project_title'];
          break;

        case 'stormticket':
          $project_title = $row['ticket_project_title'];
          break;
      }

      $key = $project_title .': '. $row['title'] .' ('. $dependable_node_types[$row['type']] .')';
      $matches[$key] = $key;
      $_SESSION['STORM_DEPENDENCIES_AUTOCOMPLETE_PARENT_NODES'][$key] = $row['nid'];
    }
  }

  if (!empty($matches)) {
    asort($matches, SORT_LOCALE_STRING);
  }


  drupal_json($matches);
}


/**
 * save the parent node of the ticket
 * @return unknown_type
 */
function storm_dependencies_save_parent_node($nid, $vid, $parent_nid) {

  if (!empty($nid) && is_numeric($nid) && !empty($vid) && is_numeric($vid) && !empty($parent_nid) && is_numeric($parent_nid)) {

    $object = new stdClass();
    $object->nid      = $nid;
    $object->vid      = $vid;
    $object->parent_nid    = $parent_nid;

    $update = array();
    $count = db_result(db_query("SELECT COUNT(nid) FROM {storm_dependencies} WHERE nid=%d AND vid=%d", $object->nid, $object->vid));
    if ($count > 0) {
      $update[] = 'nid';
      $update[] = 'vid';
    }
    drupal_write_record('storm_dependencies', $object, $update);

  }

}


/**
 * delete the parent node
 *
 * @param unknown_type $node
 * @return unknown_type
 */
function storm_dependencies_delete_parent_node($node) {

  if (!empty($node->vid)) {

    $args = array();
    $sql  = 'DELETE FROM {storm_dependencies} WHERE ';
    $sql .= 'vid = %d ';
    $args[] = $node->vid;
    db_query(db_rewrite_sql($sql, 'storm_dependencies', 'vid', $args), $args);

  }

}

/**
 * load the parent node of the ticket
 * @return unknown_type
 */
function storm_dependencies_load_parent_node($node) {

  if (!empty($node->vid)) {
    $dependable_node_types = storm_dependencies_dependable_node_types();

    $sql  = 'SELECT std.parent_nid, n.title, n.type ';
    $sql .= 'FROM {node} n ';
    $sql .= 'LEFT JOIN {storm_dependencies} std ON std.parent_nid = n.nid ';
    $sql .= 'WHERE std.vid = %d ';
    $result = db_query(db_rewrite_sql($sql, 'std', 'vid'), $node->vid);
    if (!empty($result)) {
      while ($row = db_fetch_array($result)) {

        $project_title = '';
        $table = '';

        switch ($row['type']) {

          case 'stormtask':
            $project_title = $row['task_project_title'];
            $table = 'stormtask';
            break;

          case 'stormticket':
            $project_title = $row['ticket_project_title'];
            $table = 'stormticket';
            break;
        }

        $sql_project  = 'SELECT st.project_title ';
        $sql_project .= 'FROM {node} n ';
        $sql_project .= 'LEFT JOIN {storm_dependencies} std ON std.parent_nid = n.nid ';
        $sql_project .= 'LEFT JOIN {%s} st ON n.vid = st.vid ';
        $sql_project .= 'WHERE std.vid = %d ';
        $project_title = db_result(db_query(db_rewrite_sql($sql_project, 'st', 'project_title'), $table, $node->vid));

        $key = '';
        if (!empty($project_title)) {
          $key = $project_title .': ';
        }
        $key .= $row['title'] .' ('. $dependable_node_types[$row['type']] .')';

        $node->parent_stormnode = $key;
        $node->parent_stormnode_title = $row['title'];
        $node->parent_stormnode_nid = $row['parent_nid'];
        $node->parent_stormnode_type = $dependable_node_types[$row['type']];

      }
    }
  }

}

/**
 * load the title of parent node
 * @return unknown_type
 */
function storm_dependencies_load_dependencies($vid, $return_type = 'title') {

  if ('array' == $return_type) {
    $sql  = 'SELECT n.title, std.parent_nid ';
  }
  else {
    $sql  = 'SELECT n.title ';
  }

  $sql .= 'FROM {node} n ';
  $sql .= 'LEFT JOIN {storm_dependencies} std ON std.parent_nid = n.nid ';
  $sql .= 'WHERE std.vid = %d ';
  $result = db_query(db_rewrite_sql($sql, 'std', 'vid'), $vid);

  if ('array' == $return_type) {
    while ($row = db_fetch_array($result)) {
      $parent = array();
      $parent['parent_nid'] = $row['parent_nid'];
      $parent['title'] = $row['title'];
    }
    return $parent;
  }
  else {
    return db_result($result);
  }
}

/**
 * Implementation of hook_nodeapi()
 * @return unknown_type
 */
function storm_dependencies_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  if ('stormticket' != $node->type && 'stormtask' != $node->type) {
    return;
  }

  switch ($op) {

    // LOAD NODE
    case 'load':
    case 'prepare':
      storm_dependencies_load_parent_node($node);
      break;

    // INSERT
    case 'insert':
    // UPDATE
    case 'update':
      if (!empty($node->parent_stormnode)) {
        $parent_nid = $_SESSION['STORM_DEPENDENCIES_AUTOCOMPLETE_PARENT_NODES'][$node->parent_stormnode];
        if (empty($parent_nid) || !is_numeric($parent_nid)) {
          $parent_nid = $node->parent_stormnode_nid;
        }
        if (!empty($parent_nid) && is_numeric($parent_nid)) {
          storm_dependencies_save_parent_node($node->nid, $node->vid, $parent_nid);
        }
      } else {
        storm_dependencies_delete_parent_node($node);
      }
      unset($_SESSION['STORM_DEPENDENCIES_AUTOCOMPLETE_PARENT_NODES']);

      break;

   // VALIDATE
    case 'validate':
      storm_dependencies_validate_dates($node);
      break;

    // DELETE
    case 'delete':
      // DELETE THE PARENT NODE DEPENDENCY
      storm_dependencies_delete_parent_node($node);
      break;

    // VIEW NODE
    case 'view':
      $node->content['storm_dependencies'] = array(
      '#value' => storm_dependencies_display_parent_node($node),
      );
      break;

  }

}


function storm_dependencies_display_parent_node($node) {

  $content = '';

  if (!empty($node)) {
    $content = theme('storm_dependencies_display_parent_node', $node);
  }

  return $content;
}


/**
 * Validate start and end date if the ticket/task is depending on another node
 *
 * @param unknown_type $node
 * @return unknown_type
 */
function storm_dependencies_validate_dates($node) {

  if (!empty($node->parent_stormnode)) {

    $date_begin = storm_contrib_common_date_to_time($node->datebegin);
    $date_end = storm_contrib_common_date_to_time($node->dateend);

    // IF PARENT NODE WAS PREVIOUSLY ASSIGNED
    if (!empty($node->parent_stormnode_nid)) {

      $parent_node = node_load($node->parent_stormnode_nid);
      $parent_date_begin = storm_contrib_common_date_to_time($parent_node->datebegin);
      $parent_date_end = storm_contrib_common_date_to_time($parent_node->dateend);

    }
    // IF NEW ASSIGNMENT
    else {

      $parent_node_nid = $_SESSION['STORM_DEPENDENCIES_AUTOCOMPLETE_PARENT_NODES'][$node->parent_stormnode];

      if (!empty($parent_node_nid)) {
        $parent_node = node_load($parent_node_nid);
        $parent_date_begin = storm_contrib_common_date_to_time($parent_node->datebegin);
        $parent_date_end = storm_contrib_common_date_to_time($parent_node->dateend);
      }

    }

    // CURRENT TICKET BEGINS BEFORE TO PARENT TICKET
    if ($date_begin < $parent_date_begin) {
      form_set_error('datebegin', t('This ticket depends on %value_parent_ticket. This ticket can not start before the parent ticket starts. - Start date of parent ticket: %value_parent_date',
      array('%value_parent_ticket' => $node->parent_stormnode,
      '%value_parent_date' => storm_contrib_common_time_to_date($parent_node->datebegin, storm_contrib_common_get_custom_display_date_format())  )));
    }

  }

}


/**
 * check for dependencies for this node
 * to other projects, tickets, tasks, timetrackings
 *
 * @param unknown_type $node
 */
function storm_dependencies_check_4_dependencies($node) {

  $found = array();

  if (!empty($node)) {

    // -----------------------------------------------
    // CHECK FOR DEPENDENCY TO OTHER TICKET OR TASK
    // -----------------------------------------------
    if (!empty($node->parent_stormnode_nid)) {
      // NODE HAS DEPENDENCY TO ANOTHER TASK OR TICKET
      $confirm_delete = FALSE;
      $found['dependency'] = array(
        'key' => $node->parent_stormnode,
        'title' => $node->parent_stormnode_title,
        'nid' => $node->parent_stormnode_nid,
        'type' => $node->parent_stormnode_type,
      );
    }


    // ---------------------------------
    // CHECK FOR FURTHER DEPENDENCIES
    // ---------------------------------

    switch ($node->type) {

      // STORMTICKET
      case 'stormticket':

        // CHECK FOR ASSIGNED TIMETRACKINGS
        if (module_exists('stormtimetracking') && 'stormtimetracking' != $node->type) {
          $timerackings = stormtimetracking_extension_get_trackings_of_type($node, FALSE, 0, 0, 'flat');
          if (!empty($timerackings)) {
            $confirm_delete = FALSE;
            $found['timetrackings'] = $timerackings;
          }
        }

        break;

      // STORMTASK
      case 'stormtask':

        // CHECK FOR ASSIGNED TIMETRACKINGS
        if (module_exists('stormtimetracking') && 'stormtimetracking' != $node->type) {
          $timerackings = stormtimetracking_extension_get_trackings_of_type($node, FALSE, 0, 0, 'flat');
          if (!empty($timerackings)) {
            $confirm_delete = FALSE;
            $found['timetrackings'] = $timerackings;
          }
        }

        // CHECK IF TICKETS  ARE ASSIGNED
        $filter = array($node->type => $node->nid);
        $tickets = storm_contrib_common_get_stormtickets($filter);
        if (!empty($tickets)) {
          $confirm_delete = FALSE;
           $found['tickets'] = $tickets;
        }

        break;

      // STORMPROJECT
      case 'stormproject':

        // CHECK FOR ASSIGNED TIMETRACKINGS
        if (module_exists('stormtimetracking') && 'stormtimetracking' != $node->type) {
          $timerackings = stormtimetracking_extension_get_trackings_of_type($node, FALSE, 0, 0, 'flat');
          if (!empty($timerackings)) {
            $confirm_delete = FALSE;
            $found['timetrackings'] = $timerackings;
          }
        }

        // CHECK FOR ASSIGNED TASKS
        $filter = array($node->type => $node->nid);
        $tasks = storm_contrib_common_get_stormtasks($filter);
        if (!empty($tasks)) {
          $confirm_delete = FALSE;
          $found['tasks'] = $tasks;
        }

        // CHECK FOR ASSIGNED TICKETS
        $filter = array($node->type => $node->nid);
        $tickets = storm_contrib_common_get_stormtickets($filter);
        if (!empty($tickets)) {
          $confirm_delete = FALSE;
          $found['tickets'] = $tickets;
        }

        break;

      // STORMORGANIZATION
      case 'stormorganization':

        // CHECK FOR ASSIGNED TIMETRACKINGS
        if (module_exists('stormtimetracking') && 'stormtimetracking' != $node->type) {
          $timerackings = stormtimetracking_extension_get_trackings_of_type($node, FALSE, 0, 0, 'flat');
          if (!empty($timerackings)) {
            $confirm_delete = FALSE;
            $found['timetrackings'] = $timerackings;
          }
        }

        // CHECK FOR ASSIGNED PROJECTS
        $filter = array($node->type => $node->nid);
        $projects = storm_contrib_common_get_stormprojects($filter);
        if (!empty($projects)) {
          $confirm_delete = FALSE;
          $found['projects'] = $projects;
        }

        // CHECK FOR ASSIGNED TASKS
        $filter = array($node->type => $node->nid);
        $tasks = storm_contrib_common_get_stormtasks($filter);
        if (!empty($tasks)) {
          $confirm_delete = FALSE;
          $found['tasks'] = $tasks;
        }

        // CHECK FOR ASSIGNED TICKETS
        $filter = array($node->type => $node->nid);
        $tickets = storm_contrib_common_get_stormtickets($filter);
        if (!empty($tickets)) {
          $confirm_delete = FALSE;
          $found['tickets'] = $tickets;
        }

        break;

    }

  }

  return $found;
}

/**
 * captured callback of node delete confirm
 * the aim is to avoid deleting of nodes with dependencies to timetrackings etc
 * @param unknown_type $node
 */
function storm_dependencies_node_delete_confirm($node) {

  if (!empty($node)) {

    $confirm_delete = TRUE;
    $found = array();

    $dependencies = storm_dependencies_check_4_dependencies($node);
    if (!empty($dependencies)) {
      $confirm_delete = FALSE;
    }

    // IF NO DEPENDENCES OR ASSIGNMENTS ARE FOUND
    // OR IF SET IN ADMIN SETTINGS TO AVOID NODE DELETING IF NODE HAS DEPENDENCIES OR ASSIGNMENTS
    if ($confirm_delete || !storm_dependencies_avoid_deleting_of_node_if_dependencies()) {
      // STANDARD NODE DELETE CONFIRM
      $content .= drupal_get_form('node_delete_confirm', $node);
    }
    else {

      if (!empty($dependencies)) {

        if (count($dependencies) > 1) {
          $keys = array_keys($dependencies);
          $last = array_pop($keys);
          $types = implode(', ', $keys);
          drupal_set_message(t('This @type "%name" must not be deleted because of dependencies to @types and @last. Click !link for details.',
          array('@type' => str_replace('storm', '', $node->type), '%name' => $node->title, '@types' => $types, '@last' => $last,
          '!link' => l(t('here'), 'node/'. $node->nid .'/dependencies')) ), 'error');
        }
        else {
          $types = implode(', ', array_keys($dependencies));
          drupal_set_message(t('This @type "%name" must not be deleted because of dependencies to @types. See !link for details.',
          array('@type' => str_replace('storm', '', $node->type), '%name' => $node->title, '@types' => $types,
          '!link' => l(t('here'), 'node/'. $node->nid .'/dependencies')) ), 'error');
        }

      }

      // REDIRECT BACK TO NODE
      drupal_goto('node/'. $node->nid);

    }

  }

  return $content;
}

/**
 * display dependencies and assignments to node
 *
 * @param unknown_type $node
 */
function storm_dependencies_display_node_dependencies_and_assignments($node) {

  $content = '';

  if (!empty($node)) {

    drupal_set_title(t('Dependencies for !title', array('!title' => l($node->title, 'node/'.$node->nid))));

    $dependencies = storm_dependencies_check_4_dependencies($node);

    $content .= theme('storm_dependencies_display_node_dependencies_and_assignments', $node, $dependencies);

  }

  return $content;
}
