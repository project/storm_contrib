<?php

/*
 * @file storm_dependencies.theme.inc
 * 
 * 
 */


/**
 * theme parent ticket
 * 
 * @param unknown_type $node
 * @return unknown_type
 */
function theme_storm_dependencies_display_parent_node($node) {
  
  $content  = '';
  
  if (!empty($node->parent_stormnode_title)) {
    $content .= '<div class="stormbody">';
    $content .= theme('storm_view_item', t('Depends on'), l($node->project_title, 'node/'. $node->project_nid) .' - '. l($node->parent_stormnode_title, 'node/'. $node->parent_stormnode_nid) .' ('. $node->parent_stormnode_type .')');
    $content .= '</div>';
  }
  return $content;
}


/** 
 * display dependencies and assignments to node
 * @param unknown_type $node
 * @param unknown_type $dependencies
 */
function theme_storm_dependencies_display_node_dependencies_and_assignments($node, $dependencies) {
  
  $content = '';
  
  if (!empty($dependencies)) {
    
    // --------------------
    // DISPLAY DEPENDENCY
    // ---------------------
    if (!empty($dependencies['dependency'])) {
      $content .= '<h3>'. t('Depends on') .'</h3>';
      $content .= '<ul>';
      $content .= '<li>';
      $content .= l($dependencies['dependency']['key'], 'node/'. $dependencies['dependency']['nid']);
      $content .= '</li>';
      $content .= '</ul>';
      $content .= "<br />\n";
    }
    
    // ---------------------
    // DISPLAY ASSIGNMENTS
    // ---------------------
    
    $display_array = array(
    'tasks',
    'tickets',
    'timetrackings',
    );
    
    if (!empty($display_array)) {
      foreach ($display_array as $type) {
        
        if (!empty($dependencies[$type])) {
          
          $content .= '<h3>'. ucfirst($type) .'</h3>'; // TODO TRANSLATION ?
          $content .= '<ul>';
      
          foreach ($dependencies[$type] as $nid => $title) {
            
            if (!empty($nid) && is_numeric($nid)) {
              
              $content .= '<li>';
              $content .= l($title, 'node/'. $nid);
              $content .= '</li>';
              
            }
            
          }
          
          $content .= '</ul>';
          $content .= "<br />\n";
          
        }
        
      }
    }
    
    
  }
  else {
    $content .= t('No dependencies or assignments available');
  }
  
  return $content;
}