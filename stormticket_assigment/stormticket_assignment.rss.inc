<?php

/**
 * menu callback to create rss items
 *
 * @param  $account
 * @return void
 */
function stormticket_assignment_modification_rss($account) {

  global $base_url;

  $language = user_preferred_language($account);

  $nids = array();
  // if unauthorized (401 - waiting on user input) only show channel, but no items
  if ((!isset($GLOBALS['stormticket_assignment_unauthorized']) || !$GLOBALS['stormticket_assignment_unauthorized']) && stormticket_assignment_rss_track_history_enabled()) {
    $result =  db_query_range("
      SELECT h.nid, h.vid, h.old_vid, h.modifier_uid, h.op, h.timestamp, h.title, h.modifications
       FROM {stormticket_assignment_modification_history} h
       JOIN {node} n ON (h.nid = n.nid AND h.vid = n.vid)
       LEFT JOIN {stormticket} st ON (h.nid = st.nid)
       LEFT JOIN {stormtask} stt ON (h.nid = stt.nid)
       WHERE (st.assigned_nid = %d OR stt.assigned_nid = %d OR n.uid = %d) ORDER BY h.timestamp DESC",
      $account->stormperson_nid, $account->stormperson_nid, $account->uid, 0, variable_get('feed_default_items', 10));
    while ($row = db_fetch_array($result)) {
      $nids[$row['nid']] = $row;
      $nids[$row['nid']]['modifications'] = unserialize($row['modifications']);
    }
  }

  $namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');

  $items = '';
  foreach ($nids as $nid => $history) {
    $history['account'] = &$account;
    if ($history['op'] != 'delete') {
      $item = node_load($nid, $history['vid']);
      $item->build_mode = NODE_BUILD_RSS;
      $item->link = url("node/$nid", array('absolute' => TRUE));

      $extra = node_invoke_nodeapi($item, 'rss item');
      $extra = array_merge($extra, array(array('key' => 'pubDate', 'value' => gmdate('r', $item->created)), array('key' => 'dc:creator', 'value' => $item->name), array('key' => 'guid', 'value' => $item->nid .'('.$item->vid.')'.' at '. $base_url, 'attributes' => array('isPermaLink' => 'false'))));
      foreach ($extra as $element) {
        if (isset($element['namespace'])) {
          $namespaces = array_merge($namespaces, $element['namespace']);
        }
      }

      stormticket_assignment_format_rss_item($item, $history);

    }
    else {
      $item = stormticket_assignment_format_deleted_rss_item($history);
      $modifier_account = user_load($history['modifier_uid']);
      $extra = array(array('key' => 'pubDate', 'value' => gmdate('r', $history['timestamp'])), array('key' => 'dc:creator', 'value' => $modifier_account->name), array('key' => 'guid', 'value' => $history['nid'].'('.$history['vid'].')' .' at '. $base_url, 'attributes' => array('isPermaLink' => 'false')));
    }

    drupal_alter('stormticket_assignment_rss', $item, $history);

    $items .= format_rss_item($item->title, $item->link, $item->body, $extra);

  }

  $node_person = node_load($account->stormperson_nid);
  $person_name = !empty($node_person->fullname) ? $node_person->fullname : $node_person->title;

  $channel = array(
    'version'     => '2.0',
    'title'       => variable_get('site_name', 'Cocomore Drupal'),
    'link'        => $base_url,
    'description' => t('Latest modification and assigenments of !name\'s Tickets and Tasks', array('!name' => $person_name)),
    'language'    => $language->language
  );

  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  $output .= "<rss version=\"". $channel["version"] ."\" xml:base=\"". $base_url ."\" ". drupal_attributes($namespaces) .">\n";
  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language']);
  $output .= "</rss>\n";

  drupal_set_header('Content-Type: application/rss+xml; charset=utf-8');
  print $output;

}

/**
 * default rss body
 *
 * @return string
 */
function stormticket_assignment_node_modifications_rss_default_body() {
  // BODY
  $body  = '';

  $body .= t('!modifier_link has !op the !node_type !node_title_link');
  $body .= "<br />\n";
  $body .= t('Modifications:');
  $body .= "<br />\n";
  $body .= "!node_modifications";
  return $body;
}

/**
 * default rss item modification title
 *
 * @return string
 */
function stormticket_assignment_node_modifications_rss_default_title() {
  return t('!modifier_name !op !node_type "!node_title"');
}

/**
 * default rss item new assignment title
 *
 * @return string
 */
function stormticket_assignment_new_assignment_rss_default_title() {
  return t('The !node_type "!node_title" has been assigned to !node_person_fullname');
}

/**
 * format the rss item, replace body and site tokens with their values
 *
 * @param  $item
 * @param array $params
 * @return void
 */
function stormticket_assignment_format_rss_item(&$item, $params = array()) {
  $stormticket_assignment_get_node_modifications_rss_body = variable_get('stormticket_assignment_node_modifications_rss_body',  stormticket_assignment_node_modifications_rss_default_body());

  if (!empty($params) && !empty($item)) {
    $account = $params['account'];

    $language = user_preferred_language($account);

    $node_person = node_load($account->stormperson_nid);

    $vars = array_merge(
      user_mail_tokens($account, $language),
      stormticket_assignment_ticket_mail_tokens($item, $language, 'html'),
      stormticket_assignment_person_mail_tokens($node_person, $language, 'html')
    );

    $modifier_account = user_load($params['modifier_uid']);
    // MODIFIER ID
    // MODIFIER LINK
    // MODIFIER NAME
    if (!empty($modifier_account->stormperson_nid)) {
      $modifier_node = node_load($modifier_account->stormperson_nid);
      $vars['!modifier_name'] = !empty($modifier_node->fullname) ? $modifier_node->fullname : $modifier_node->title;
      $vars['!modifier_id'] = $modifier_account->stormperson_nid;
      $vars['!modifier_link'] = l($vars['!modifier_name'], 'node/'. $modifier_account->stormperson_nid, array('absolute' => TRUE));
    }
    else {
      $vars['!modifier_id'] = $modifier_account->uid;
      $vars['!modifier_link'] = l($modifier_account->name, 'user/'. $modifier_account->uid, array('absolute' => TRUE));
      $vars['!modifier_name'] = $modifier_account->name;
    }

    switch ($params['op']) {

      case 'insert':
        $vars['!op'] = t('inserted');
        break;

      case 'update':
        $vars['!op'] = t('updated');
        break;

      case 'delete':
        $vars['!op'] = t('deleted');
        break;

    }

    // MODIFICATIONS
    if (!empty($params['modifications'])) {
      $modifications = array();
      foreach ($params['modifications'] as $modification_item => $modification_text) {
        $modifications[] = ucfirst($modification_item) .': '. $modification_text; // TODO TRANSLATION OF ITEM
      }
      if (!empty($modifications)) {
        $vars['!node_modifications'] = implode('<br />', $modifications);
      }
    }
    else {
      $vars['!node_modifications'] = t('no changes');
    }

    drupal_alter('stormticket_assignment_rss_token', $vars);

    $item->body = strtr($stormticket_assignment_get_node_modifications_rss_body, $vars);

    if ($node_person->nid == $item->assigned_nid) {
      $old_node = node_load($item->nid, $params['old_vid']);
      if (($params['op'] == 'insert' || (!empty($old_node) && $old_node->assigned_nid != $item->assigned_nid))) {
        $title = variable_get('stormticket_assignment_new_assignment_rss_title', stormticket_assignment_new_assignment_rss_default_title());
      }
    }
    if (empty($title)) {
      $title = variable_get('stormticket_assignment_node_modifications_rss_title', stormticket_assignment_node_modifications_rss_default_title());
    }

    $item->title = strtr($title, $vars);
  }
}

/**
 * format a deleted node rss item - we ca not load the node, because it is deleted
 * so with set all values to "", so the tokens will be replaced with an empty string
 *
 * @param  $history
 * @return stdClass
 */
function stormticket_assignment_format_deleted_rss_item($history) {
  $item = new stdClass();
  $item->build_mode = NODE_BUILD_RSS;

  $modifier_account = user_load($history['modifier_uid']);
  // MODIFIER ID
  // MODIFIER LINK
  // MODIFIER NAME
  if (!empty($modifier_account->stormperson_nid)) {
    $modifier_node = node_load($modifier_account->stormperson_nid);
    $vars['!modifier_name'] = !empty($modifier_node->fullname) ? $modifier_node->fullname : $modifier_node->title;
    $vars['!modifier_id'] = $modifier_account->stormperson_nid;
    $vars['!modifier_link'] = l($vars['!modifier_name'], 'node/'. $modifier_account->stormperson_nid, array('absolute' => TRUE));
  }
  else {
    $vars['!modifier_id'] = $modifier_account->uid;
    $vars['!modifier_link'] = l($modifier_account->name, 'user/'. $modifier_account->uid, array('absolute' => TRUE));
    $vars['!modifier_name'] = $modifier_account->name;
  }

  $vars['!op'] = t('deleted');
  $vars['!node_type'] = "";
  $vars['!node_title'] = $history['title'];
  $vars['!node_title_link'] = $history['title'];
  $vars['!node_revision_log'] = "";
  $vars['!node_date_begin'] = "";
  $vars['!node_date_end'] = "";
  $vars['!node_duration'] = "";
  $vars['!node_description'] = "";
  $vars['!node_person_fullname'] = "";
  $vars['!node_person_link'] = "";

  // MODIFICATIONS
  if (!empty($history['modifications'])) {
    $modifications = array();
    foreach ($history['modifications'] as $modification_item => $modification_text) {
      $modifications[] = ucfirst($modification_item) .': '. $modification_text; // TODO TRANSLATION OF ITEM
    }
    if (!empty($modifications)) {
      $vars['!node_modifications'] = implode('<br />', $modifications);
    }
  }
  else {
    $vars['!node_modifications'] = t('no changes');
  }

  $stormticket_assignment_get_node_modifications_rss_body = variable_get('stormticket_assignment_node_modifications_rss_body',  stormticket_assignment_node_modifications_rss_default_body());
  $title = variable_get('stormticket_assignment_node_modifications_rss_title', stormticket_assignment_node_modifications_rss_default_title());

  $item->title = strtr($title, $vars);
  $item->body = strtr($stormticket_assignment_get_node_modifications_rss_body, $vars);
  $item->link = "";

  return $item;
}