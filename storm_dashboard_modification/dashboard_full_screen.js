var Intranet = Intranet || {};

Intranet.SDB = Intranet.SDB || {};

Intranet.SDB.dashboard = {};

Intranet.SDB.dashboard.hide = function() {
  $("<div id='sdb_hide_show_button'><span>Hide menus</span></div>")
  	.css({'border':'1px solid grey',
  		  'height':'20px',
  		  'width': '80px'
  	})
  	.insertBefore(".left-corner #sdb");
  $("#sdb_hide_show_button span")
    .css({'display':'block',
    	  'height':'20px',
	  	  'text-align':'center'
	});
}

Intranet.SDB.dashboard.hideMenu = function() {
  $(".page-stormdashboard #sidebar-left").animate({
	width: 0,
  },
  2000,
  function() { 
	$(".page-stormdashboard #sidebar-left").hide(2000);
	$("body.sidebar-left #center").css("margin-left", "0px");
	$("body.sidebar-left #squeeze").css("margin-left", "0px");
  });

  $("#sdb_hide_show_button span").html("Show menus");
  $("#sdb_hide_show_button").css("margin-left", "5px");
  $(".left-corner h2").css("margin-left", "5px");
  $(".page-stormdashboard #sdb").addClass("menu-hidden");
  $(".left-corner").css({
	  'left':'0',
	  'margin-left':'0',
	  'padding':'60px 0 5em 0'
  });
  $("#sdb .left.wide").css({
	  'width':'340px',
	  'margin-left':'5px'
  });
  $("#sdb .right").css({
	  'width':'340px',
	  'margin-right':'5px'
  });
}

Intranet.SDB.dashboard.showMenu = function() {

  $("body.sidebar-left #center").css("margin-left", "-210px");
  $("body.sidebar-left #squeeze").css("margin-left", "210px");
  $(".page-stormdashboard #sidebar-left").animate({
	width: 210
  },2000);
  
  $("#sdb_hide_show_button span").html("Hide menus");
  $(".left-corner").css({
	  'left':'-10px',
	  'margin-left':'-10px',
	  'padding':'60px 0 5em 20px'
  });
  $("#sdb .left.wide").css({
	  'width':'260px',
	  'margin-left':'0'
  });
  $("#sdb .right").css({
	  'width':'260px',
	  'margin-right':'0'
  });
}

Intranet.SDB.dashboard.hideClicked = function() {
  $("#sdb_hide_show_button span").click(function() {
    var html_value = $("#sdb_hide_show_button span").html();
	  
	if (html_value == "Hide menus") {
	  Intranet.SDB.dashboard.hideMenu();
	}
	if (html_value == "Show menus") {
	  Intranet.SDB.dashboard.showMenu();
	}
  });
}

$(document).ready(function(){
	
  Intranet.SDB.dashboard.hide();
  Intranet.SDB.dashboard.hideClicked();
});
