<?php


/**
 * @file storm_contrib_common_theme.inc
 */

/**
 * theme a number
 *
 * @param unknown_type $number
 */
function theme_storm_contrib_common_number_format($number, $number_decimal = FALSE) {
  $content = '';
  if (!empty($number_decimal) || 0 == $number_decimal) {
    $content = number_format($number, storm_contrib_common_get_number_format_decimal_numbers(), storm_contrib_common_get_number_format_decimal_separator(), storm_contrib_common_get_number_format_thousands_separator());
  }
  else {
    $content = number_format($number, storm_contrib_common_get_number_format_decimal_numbers(), storm_contrib_common_get_number_format_decimal_separator(), storm_contrib_common_get_number_format_thousands_separator());
  }
  return $content;
}

/**
 * format a duration - %duration %unit
 * when export module is active, it will export only %duration in hours
 *
 * @param int/float $duration
 * @param string $unit one of 'hour', 'day'
 */
function theme_storm_contrib_common_format_plural_duration($duration, $unit = 'hour', $convert_export = 'hour') {
  $content = '';
  if (module_exists('storm_exports') && !empty($_SESSION['storm_exports_export_'. request_uri() ])) {
    // format for export
    if ($convert_export == 'hour' && $unit != 'hour') {
      $duration = storm_contrib_common_calculate_duration_in_hours($unit, $duration);
    }
    $content = (string) theme('storm_contrib_common_number_format', $duration);
  }
  else {
    // format for inline
    switch ($unit) {
      case 'hour':
        $format_singular = '@count hour';
        $format_plural = '@count hours';
        break;
      case 'day':
        $format_singular = '@count day';
        $format_plural = '@count days';
        break;
    }
    $content = format_plural(theme('storm_contrib_common_number_format', $duration), $format_singular, $format_plural);
  }
  return $content;
}


function theme_storm_contrib_common_progress_bar($progress, $duration = 1, $width=NULL, $height=NULL) {
  if ($progress == 0 && $duration == 0) {
    return t('n.a.');
  }
  $progress = $progress * 100;

  if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
    return theme('storm_contrib_common_number_format', $progress).'%';
  }
  drupal_add_css(drupal_get_path('module', 'storm_contrib_common') .'/storm_contrib_common.css');

  $output  = '';
  $ext_class = '';
  $style = '';
  if (!empty($width)) {
    $style = 'width: '.$width.'px;';
  }
  if (!empty($height)) {
    $style .= 'height: '.$height.'px;';
  }
  if (!empty($style)) {
    $style = ' style="'.$style.'"';
  }
  $output  = '<div class="storm_contrib_progress_bar"'.$style.'>';
  $width = round($progress);
  if ($width > 100) {
    $width= 100;
    $ext_class = 'storm_contrib_progress_bar_over';
  }
  $output .= '<div class="storm_contrib_progress_bar_fill '.$ext_class.'" style="width: '.$width.'%">';
  $output .= '<span>'.theme('storm_contrib_common_number_format', $progress). "%".'</span>';
  $output .= '</div>';

  $output .= '</div>';

  return $output;
}

function theme_storm_contrib_common_progress_bar_pure($progress, $duration = 1, $width=NULL, $height=NULL) {

  drupal_add_css(drupal_get_path('module', 'storm_contrib_common') .'/storm_contrib_common.css');

  $output  = '';
  $ext_class = '';
  $style = '';
  if (!empty($width)) {
    $style = 'width: '.$width.'px;';
  }
  if (!empty($height)) {
    $style .= 'height: '.$height.'px;';
  }
  if (!empty($style)) {
    $style = ' style="'.$style.'"';
  }
  $output  = '<div class="storm_contrib_progress_bar"'.$style.'>';
  $width = round($progress);
  if ($width > 100) {
    $width= 100;
    $ext_class = 'storm_contrib_progress_bar_over';
  }
  $output .= '<div class="storm_contrib_progress_bar_fill '.$ext_class.'" style="width: '.$width.'%">';
  $output .= '</div>';

  $output .= '</div>';

  return $output;
}


function theme_storm_contrib_common_add_nid($node) {
  $content  = '';
  $content .= '<div class="stormfields">';
  $content .= theme('storm_view_item', t('ID'), '<span class="'.$node->type.'-id">'. $node->nid .'</span>');
  $content .= '</div>';
  return $content;
}

function theme_storm_contrib_common_menu_item_link($link) {
  drupal_add_css(drupal_get_path('module', 'storm_contrib_common') .'/storm_contrib_common_menu.css');
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  $extras = "";
  if (!empty($link['options']['extra'])) {
    $tmp = array();
    foreach ($link['options']['extra'] as $e) {
      if ((!empty($e['permission']) && user_access($e['permission'])) || empty($e['permission'])) {
        $tmp[] = l(t($e['title']), $e['href']);
      }
    }
    if (count($tmp) > 0) {
      $extras .= ' <span class="menu-extra">[ '.implode(' | ', $tmp).' ]</span>';
    }
    unset($link['options']['extras']);
  }

  return l($link['title'], $link['href'], $link['localized_options']).$extras;

}