<?php

/**
 * @file stormperson_extension.theme.inc
 *
 * theme functions for module stormpersons_extension
 */


/**
 * theme person page view add ons
 *
 * @param unknown_type $total_duration
 * @param unknown_type $latest_end_date
 * @return unknown_type
 */
function theme_stormperson_extension_page_view_adds($node) {
  $content = '';

  drupal_add_css(drupal_get_path('module', 'stormperson_extension') . '/stormperson_exension.css');

  // DEPARTMENTS
  if (!empty($node->department_terms)) {
    $departments = array();
    foreach ($node->department_terms as $department_term) {
      $departments[] = $department_term->name;
    }

    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Department'), implode(', ', $departments));
    $content .= '</div>';

  }

  // SKILLS
  if (!empty($node->skills)) {
    $skills = array();
    foreach ($node->skills as $skill) {
      $path = taxonomy_term_path($skill);
      $skills[] = l($skill->name, $path);
    }

    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Skills'), implode(', ', $skills));
    $content .= '</div>';

  }

  // TEAMS
  if (!empty($node->teams) && !variable_get('stormperson_extension_hide_teams', FALSE)) {
    $skills = array();
    foreach ($node->teams as $team_nid => $team_name) {
      $teams[] = l($team_name, 'node/'. $team_nid);
    }

    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Teams'), implode(', ', $teams));
    $content .= '</div>';

  }

  // PICTURE
  if (!empty($node->user_picture)) {
    $content .= '<div class="stormperson-extension-user-picture">';
    $content .= theme('user_picture', user_load($node->user_uid));
    $content .= '</div>';
  }

  // ONLINE STATUS
  if (!empty($node->online_status)) {
    $content .= '<div class="stormfields">';
    $tmp = '';
    $tmp .= '<span class="stormperson-extension-user-online-online">'. $node->online_icon .'</span>';
    $tmp .= '<span class="stormperson-extension-user-online-status">'. $node->online_status .'</span>';
    $content .= theme('storm_view_item', t('Online status'), $tmp);
    $content .= '</div>';

  }

  // HIRED TILL
  if (!empty($node->hired_till)) {
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Hired till'), storm_contrib_common_time_to_date(storm_contrib_common_timestamp_to_timezone($node->hired_till), storm_contrib_common_get_custom_display_date_format()) );
    $content .= '</div>';
  }

  return $content;
}


function theme_stormperson_extension_not_work_anymore($node) {
  $content  = '';
  $content .= '<div class="stormfields">';
  $content .= '<strong>'.t('This person does not work here anymore.').'</strong>';
  $content .= '</div>';
  return $content;
}