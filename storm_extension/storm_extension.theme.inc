<?php

/**
 * @file storm_extension.theme.inc
 */



/**
 * theme storm dashboard
 * @return unknown_type
 */
function theme_storm_extension_dashboard($link_blocks) {

  $content = '';

  $content .= '<div id="stormdashboard">';

  if (!empty($link_blocks)) {

    $content .= '<dl class="stormdashboard clear-block">';

    foreach ($link_blocks as $block_id => $link_block_array) {

      $content .= '<div class="stormdashboard">';

      if (!empty($link_block_array)) {
        foreach ($link_block_array as $key => $link_array) {

          if (!empty($link_array['theme'])) {
            $content .= theme($link_array['theme'], $link_array);
          }
        }
      }

      $content .= '</div>';

    }

    $content .= '</dl>';

  }
  else {
    $content .= t('No dashboard links available');
  }

  $content .= '</div>';

  return $content;
}


/**
 * theme the dashboard as top navigation block
 * @param unknown_type $link_blocks
 */
function theme_storm_extension_dashboard_top_navigation($link_blocks) {

  $content = '';

  $content .= '<div id="stormdashboard-top-navigation" class="clear-block">';

  if (!empty($link_blocks)) {

//    $content .= '<dl class="stormdashboard-top-navigation clear-block">';

    foreach ($link_blocks as $block_id => $link_block_array) {

      if (!empty($link_block_array)) {
        foreach ($link_block_array as $key => $link_array) {

          $content .= '<div class="stormdashboard-icon">';

          if (!empty($link_array['theme'])) {
            $content .= theme($link_array['theme'], $link_array);
          }

          $content .= '</div>';
        }
      }

    }

//    $content .= '</dl>';

  }
  else {
    return "";
  }

  $content .= '</div>';

  return $content;

}


/**
 * theme storm dashboard link
 *
 * @param unknown_type $source_module
 * @param unknown_type $destination_module
 * @param unknown_type $node_nid
 * @param unknown_type $weight
 * @return unknown_type
 */
function theme_storm_extension_dashboard_link($link_array) {

  $content = '';

  // DEFAULT ICON
  if (empty($link_array['icon'])) {
    $dt_id = 'stormexpenses';
  }
  else {
    $dt_id = $link_array['icon'];
  }

  if (empty($link_array['nid']) || 0 == $link_array['nid']) {
    $params = array();
  }
  else {
    if (!empty($link_array['nid'])) {
      $params_key = $link_array['node_type'] .'_nid';
      $params['query'] = array($params_key => $link_array['nid']);
    }
  }

  $link = l($link_array['title'], $link_array['path'], $params);

  // ADD PLUS SIGN (node/add)
  if (!empty($link_array['add_type'])) {
    $item = new stdClass();
    $item->type = $link_array['add_type'];
    if (empty($link_array['params'])) {
      $link_array['params'] = array();
    }
    $link .= storm_icon_add('node/add/'. str_replace('_', '-', $link_array['add_type']), $item, $link_array['params']);
  }


  if (empty($link_array['nid']) || 0 == $link_array['nid']) {
    if (variable_get('storm_icons_display', TRUE)) {
      $content .= '<dt id="'. $dt_id .'" class="stormcomponent">';
    }
    else {
      $content .= '<dt class="stormcomponent">';
    }
    $content .= $link;
    $content .= '</dt>';
  }
  else {
    $content = array(
      '#prefix' => variable_get('storm_icons_display', TRUE) ? '<dt id="'. $dt_id .'" class="stormcomponent">' : '<dt class="stormcomponent">',
      '#suffix' => '</dt>',
      '#value' => $link,
      '#weight' => $link_array['weight'],
    );
  }


  return $content;
}


/**
 * theme storm reports dashboard
 * @return unknown_type
 */
function theme_storm_extension_storm_reports_dashboard($link_blocks) {

  $content = '';

  $content .= '<div id="stormdashboard">';

  if (!empty($link_blocks)) {

    $content .= '<dl class="stormdashboard">';

    foreach ($link_blocks as $block_id => $link_block_array) {

      $content .= '<div class="stormdashboard">';

      if (!empty($link_block_array)) {
        foreach ($link_block_array as $key => $link_array) {
          $content .= theme($link_array['theme'], $link_array);
        }
      }

      $content .= '</div>';

    }

    $content .= '</dl>';

  }
  else {
    $content .= t('No reports available');
  }

  $content .= '</div>';

  return $content;
}


/**
 * theme storm extension reports dashboard link
 *
 * @param unknown_type $source_module
 * @param unknown_type $destination_module
 * @param unknown_type $node_nid
 * @param unknown_type $weight
 * @return unknown_type
 */
function theme_storm_extension_storm_reports_dashboard_link($link_array) {

  $content = '';

  // DEFAULT ICON
  if (empty($link_array['icon'])) {
    $dt_id = 'stormexpenses';
  }
  else {
    $dt_id = $link_array['icon'];
  }

  if (variable_get('storm_icons_display', TRUE)) {
    $content .= '<dt id="'. $dt_id .'" class="stormcomponent">';
  }
  else {
    $content .= '<dt class="stormcomponent">';
  }

  $content .= l($link_array['title'], $link_array['path'], $link_array['params']);

  $content .= '</dt>';

  return $content;
}

function theme_storm_extension_dashboard_links_weight_table($form = array()) {
  $dashboard = (!empty($form['#infix'])) ? $form['#infix']."_" : '';
  $rows = array();
  foreach($form as $id => &$value) {
    if ($id[0] == '#') {
      continue;
    }
    $value['storm_extension_'.$dashboard.'dashboard_link_weight_'.$id]['#attributes']['class'] = $dashboard.'dashboard-link-table-weight';

    $row = array();
    $row[] = $value['#value'];
    $row[] = drupal_render($value['storm_extension_'.$dashboard.'dashboard_link_active_'.$id]);
    if (empty($dashboard)) {
      $row[] = drupal_render($value['storm_extension_'.$dashboard.'dashboard_activate_top_navigation_'.$id]);
    }
    $row[] = drupal_render($value['storm_extension_'.$dashboard.'dashboard_link_weight_'.$id]);
    unset($value['#value']);
    if (!empty($row)) {
      $rows[] = array(
        'data' => $row,
        'class' => 'draggable',
      );
    }
  }
  if (empty($dashboard)) {
    $headers = array(t('Link'), t('active'), t('active for Top-Nav Block'), t('Weight'));
  }
  else {
    $headers = array(t('Link'), t('active'), t('Weight'));
  }

  $output = theme('table', $headers, $rows, array('id' => $dashboard.'dashboard-link-table'));

  drupal_add_tabledrag($dashboard.'dashboard-link-table', 'order', 'sibling', $dashboard.'dashboard-link-table-weight');


  $output .= drupal_render($form);

  return $output;
}
