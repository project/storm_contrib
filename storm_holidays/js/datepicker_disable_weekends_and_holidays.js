/**
 * add some callbacks to get holidays from drupal
 */
Drupal.behaviors.addDatePopupSettings = function() {
  for (var id in Drupal.settings.datePopup) {
    Drupal.settings.datePopup[id].settings.beforeShowDay = noWeekendsOrHolidays;
    Drupal.settings.datePopup[id].settings.onChangeMonthYear = storm_holidays_get_holidays;
    Drupal.settings.datePopup[id].settings.beforeShow = storm_holidays_before_show;
    // get the actual year (make first popup faster, if we already got it)
    if (Drupal.settings.storm_holidays.consider_holidays && Drupal.settings.datePopup[id].settings.defaultDate !== undefined) {
      storm_holidays_get_holidays(new Date().getFullYear(), 1, false);
    }
  }
}
// stored holidays (Date => Name)
var storm_holidays = new Object();
// received years from ajax (only to avoid calling ajax on every month change)
var storm_holidays_received = new Array();

/**
 * beforeShowDay callback from DatePicker
 * @param date
 */
function noWeekendsOrHolidays(date) {
  var noWeekend = [true, ''];
  // make weekends unpickable (if option is set in admin config)
  if(Drupal.settings.storm_holidays.consider_weekends) {
    noWeekend = $.datepicker.noWeekends(date);
  }
  // make holidays unpickable (if option is set in admin config and if it isn't al weekend already)
  if (noWeekend[0] && Drupal.settings.storm_holidays.consider_holidays) {
    return getHolidays(date);
  }
  else {
    return noWeekend;
  }
}

/**
 * check if date is a holiday or not
 * @param date Date Object
 */
function getHolidays(date) {
  if (storm_holidays[date] ) {
    if (storm_holidays[date].half_day) {
      return [true, 'ht_day', storm_holidays[date].name];
    }
    return [false, 'ft_day', storm_holidays[date].name];
  }
  else {
    return [true, ''];
  }
}

/**
 * onChangeMonthYear callback from DatePicker
 * get holidays from year from drupal (ajax call), if we want to disable date pickup from holidays
 * @param year
 * @param month
 * @param inst DatePicker instance
 */
function storm_holidays_get_holidays(year, month, inst) {
  // only make the ajax call if we didn't received it already
  if (Drupal.settings.storm_holidays.consider_holidays && jQuery.inArray(year, storm_holidays_received) == -1) {
    // we can not use $.getJSON because we need the option async false!
    $.ajax({
      url: '/storm/holidays/get-holidays-js/'+year+'/'+Drupal.settings.storm_holidays.consider_holidays,
      async: false,
      dataType: 'json',
      success: function(data) {
        storm_holidays_received.push(year);
        for (var name in data) {
          var date = new Date(data[name].date*1000);
          var obj = $.extend(data[name], {name: name, date: date});
          storm_holidays[date] = obj;
        }
      }
    });
  }
}

/**
 * beforeShow callback of DatePicker
 * get holidays of selected year
 * @param input
 * @param inst
 */
function storm_holidays_before_show(input, inst) {
  var year = 0;
  if (inst.selectedYear !== undefined && inst.selectedYear > 0) {
    year =  inst.selectedYear;
  }
  else {
    year = new Date().getFullYear();
  }
  storm_holidays_get_holidays(year, 1, inst);
}