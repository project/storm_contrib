<?php

/**
 * @file stormtimetracking_favorites.theme.inc
 */


/**
 * theme the favorites form (add or emove button)
 *
 * @param unknown_type $form
 * @return unknown_type
 */
function theme_stormtimetracking_favorites_form($form) {

  $content = '';

  if (!empty($form)) {

    // DISPLAY AS STORMLINK
    if (!empty($form['display_as_stormlink']['#value'])) {
      $class = 'stormlinks';
    }
    // DISPLAY NORMAL
    else {
      $class = 'stormtimetracking-favorites-form';
    }

    $content .= '<div class="'. $class .'"><dl>';
    $content .= drupal_render($form);
    $content .= '</dl></div>';

  }

  return $content;
}


/*+
 * theme the list of users timetracking favorites
 *
 */
function theme_stormtimetracking_favorites_list($user, $header = array()) {

  $content = '';
  $filter_options = '';

  if (!empty($user->stormtimetracking_favorites)) {

    drupal_add_css(drupal_get_path('module', 'stormtimetracking_favorites') .'/stormtimetracking_favorites.css');

    $content .= '<div id="stormtimetracking-favorites">';

    if (empty($header)) {
      $header = array(
        array('data' => t('Organization')),
        array('data' => t('Project')),
        array('data' => t('Title')),
        array('data' => t('Type')),
        array('data' => t('Options')),
        );
    }

    $rows  = array();
    $organization_counter = array();

    foreach ($user->stormtimetracking_favorites as $favorite_nid => $favorite_array) {

      $node = new stdClass();
      $node = (object) $favorite_array;

      if (empty($organization_counter[$node->organization_nid])) {
        $organization_counter[$node->organization_nid] = array('count' => 1, 'organization_title' => $node->organization_title);
      }
      else {
        $organization_counter[$node->organization_nid]['count'] ++;
      }

      $row = array();
      $row[] = array('data' => l($node->organization_title, 'node/'. $node->organization_nid));
      $row[] = array('data' => l($node->project_title, 'node/'. $node->project_nid));
      if ('stormproject' == $node->type) {
        $row[] = array('data' => ' ');
      }
      else {
        $data  = l($node->title, 'node/'. $node->nid) ."<br />\n";
        $data .= '<div id="stormtimetracking-favorites-info">';
        $data .= t('Begin: @begin - End: @end - Duration: @duration', array(
        '@begin' => storm_contrib_common_time_to_date($node->datebegin, storm_contrib_common_get_custom_display_date_format()),
        '@end' => storm_contrib_common_time_to_date($node->dateend, storm_contrib_common_get_custom_display_date_format()),
        '@duration' => format_plural($node->duration, '@count @unit', '@count @units', array('@unit' => $node->durationunit)),
        ));
        $data .= '</div>';
        $row[] = array('data' => $data);
      }
      $row[] = array('data' => t(drupal_ucfirst(str_replace('storm', '', $node->type))));

      $tmp = array('data' => '');
      
      // load the real node to provide all access rights needed
      $node = node_load($favorite_array['nid']); 

      // TIMETRACKING TRIGGER ICON
      if (node_access('view', $node) || 1 == $user->uid) {
	      if (module_exists('storm_dashboard')) {
	        $tmp['data'] .= _storm_dashboard_timetracking_trigger(TRUE, $node->nid) .' ';
	      }
      }

      // ADD ICON
      if (node_access('view', $node) || 1 == $user->uid) {
        $item = new stdClass();
        $item->type = 'stormtimetracking';
        $link_array['params'] = array(str_replace('storm', '', $node->type) .'_nid' => $node->nid, 'destination' => $_GET['q']);
        if (module_exists('storm')) {
          $tmp['data'] .= storm_icon_add('node/add/'. str_replace('_', '-', $item->type), $item, $link_array['params']) .' ';
        }
      }

      // EDIT ICON
      if (module_exists('storm')) {
        if (node_access('update', $node) || (1 == $user->uid) ) {
          $tmp['data'] .=  storm_icon_edit_node($node, $_GET) .' ';
        }
      }

      $tmp['class'] = 'storm_list_operations';

      // REMOVE BUTTON
      $tmp['data'] .= drupal_get_form('stormtimetracking_favorites_form', $node, FALSE);

      $row[] = $tmp;

      $rows[] = array('data' => $row);

      unset($node);

    }

    if (!empty($rows)) {
      $content .= theme('table', $header, $rows);
    }

    if (count($user->stormtimetracking_favorites) > 10) {
      $items = array();
      foreach($organization_counter as $nid => $data) {
        $item = l($data['organization_title']." (".$data['count'].")", $_GET['q'], array('query' => 'organization_nid='.urlencode($nid)));
        $items[] = array('data' => $item);
      }
      $filter_options .= theme('item_list', $items, t('Filter'));
    }
    if (!empty($_GET['organization_nid']) || !empty($_GET['project_nid'])) {
      $filter_options .= l(t('Reset Filters'), $_GET['q']);
    }

  }
  else {
    $content .= t('No items available');
  }

  $content .= '</div>';

  return $filter_options.$content;
}