<?php

/**
 * @file stormperson_utilisation.views.inc
 */

/**
 * Implementation of hook_views_data()
 * 
 * @return unknown_type
 */
function stormperson_utilisation_views_data() {
  
  // --------------------------------------
  // TABLE STORMPERSON UTILISATION
  // --------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['stormperson_utilisation']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['stormperson_utilisation']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // numeric text field working times in minutes per day
  $data['stormperson_utilisation']['working_time_in_minutes_per_day'] = array(
    'title' => t('Person Utilisation - Working Time in Minutes Per Day'),
    'help' => t('The entered working time of a person per day in minutes.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  return $data;
}

