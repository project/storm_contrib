Drupal.behaviors.stormperson_utilisation_date_range = function() {
  //add some callbacks to get holidays from drupal
  for (var id in Drupal.settings.datePopup) {
    Drupal.settings.datePopup[id].settings.onSelect = stormperson_utilisation_date_range;
    var val = $("#"+id).val();
    if (val != "") {
      date = $.datepicker.parseDate(
          Drupal.settings.datePopup[id].settings.dateFormat ||
          $.datepicker._defaults.dateFormat,
          val, Drupal.settings.datePopup[id].settings );
      if (id == "edit-select-date-from-popup-datepicker-popup-0") {
        Drupal.settings.datePopup["edit-select-date-to-popup-datepicker-popup-0"].settings.minDate = date ;
      }
      else {
        Drupal.settings.datePopup["edit-select-date-from-popup-datepicker-popup-0"].settings.maxDate = date ;
      }
    }
  }

  $(".stormperson-utilisation-department-utilisation-person-ticket-switcher").click(function() {
    var id = false;
    var person_nid =false;
    $.each($(this).attr("class").split(" "), function() {
      if(this.indexOf("utilisation-person-") === 0) {
        id = this;
        var tmp = this.split('-');
        person_nid = tmp[3];
      }
    })
    var hide = $("#tickets-"+id).is(":visible");

    if (person_nid) {
      $("#stormperson-utilisation-department-utilisation-person-date-row-"+person_nid+" > .stormperson-utilisation-department-utilisation-persons-table-td-selected").removeClass("stormperson-utilisation-department-utilisation-persons-table-td-selected");
      $("#stormperson-utilisation-department-utilisation-person-assignment-row-"+person_nid+" > .stormperson-utilisation-department-utilisation-persons-ticket-table-td:visible").hide();
    }

    if (id && !hide) {
      $("#tickets-"+id).show();
      $(this).parent().addClass("stormperson-utilisation-department-utilisation-persons-table-td-selected");
    }
  });
}

function stormperson_utilisation_date_range( selectedDate ) {
  instance = $( this ).data( "datepicker" );
  date = $.datepicker.parseDate(
      instance.settings.dateFormat ||
      $.datepicker._defaults.dateFormat,
      selectedDate, instance.settings );

  if (this.id == "edit-select-date-from-popup-datepicker-popup-0") {
    $("#edit-select-date-to-popup-datepicker-popup-0").datepicker( "option", "minDate", date );
  }
  else {
    $("#edit-select-date-from-popup-datepicker-popup-0").datepicker( "option", "maxDate", date );
  }

}