<?php
/**
 * @file
 * main module file for Storm Contrib Logger
 */

/**
 * Implements hook_menu().
 * @return array
 */
function storm_contrib_logger_menu() {
  $menu_items = array();

  $menu_items['user/%user/activities'] = array(
    'title' => 'User Storm Activities',
    'page callback' => 'storm_contrib_logger_view_activities',
    'page arguments' => array(1),
    'access callback' => 'storm_contrib_logger_view_activities_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  $menu_items['node/%node/activities'] = array(
    'title' => 'Storm Activities',
    'page callback' => 'storm_contrib_logger_view_activities',
    'page arguments' => array(1),
    'access callback' => 'storm_contrib_logger_view_activities_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  $menu_items['storm/activities'] = array(
    'title' => 'Activities',
    'page callback' => 'storm_contrib_logger_view_activities',
    'page arguments' => array(),
    'access callback' => 'storm_contrib_logger_view_activities_access',
    'access arguments' => array(),
    'type' => MENU_NORMAL_ITEM,
  );

  return $menu_items;
}

/**
 * Implements hook_storm_contrib_common_admin_settings_form().
 * All settings are nested in one Form.
 *
 * @return array
 *  Drupal forms array
 * @see storm_contrib_common_admin_settings_form()
 */
function storm_contrib_logger_storm_contrib_common_admin_settings_form() {
  $form = array();

  $form['storm_contrib_log']['storm_contrib_logger'] = array(
    '#type' => 'fieldset',
    '#title' => t('Log Messages'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  $form['storm_contrib_log']['storm_contrib_logger']['storm_contrib_logger_delete_older'] = array(
    '#type' => 'select',
    '#title' => t('Delete older Messages'),
    '#options' => array(
      0 => t('Never'),
      86400 => t('older than a day'),
      604800 => t('older than a week'),
      2592000 => t('older than a month'),
      7776000 => t('older than three months')
    ),
    '#default_value' => variable_get('storm_contrib_logger_delete_older', 2592000),
  );

  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => t('Customize Messages'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  $msg_types = storm_contrib_log_heartbeat_message_info();
  foreach ($msg_types as $msg) {
    $mid = $msg['message_id'];
    $default_value = variable_get('storm_contrib_logger_msg_'.$mid, $msg['message']);
    $ava_vars = array();
    if (!empty($msg['variables'])) {
      foreach ($msg['variables'] as $var => $value) {
        $ava_vars[] = str_replace('@', '!', $var);
      }
    }
    $ava_vars = implode(', ', $ava_vars);
    $fieldset['storm_contrib_logger_msg_'.$mid] = array(
      '#type' => 'textfield',
      '#title' => t('Message: !mid', array('!mid' => $mid)),
      '#description' => t($msg['description'])."<br/>".t('Available Variables: !vars', array('!vars' => $ava_vars)),
      '#default_value' => $default_value,
    );
  }

  $form['storm_contrib_log']['storm_contrib_logger']['custom_messages'] = $fieldset;

  return $form;
}

/**
 * Builds the translated message from the message id and variables.
 *
 * @param string $message_id
 * @param null|array $variables
 * @return string
 *   translated message
 */
function storm_contrib_logger_build_message($message_id, $variables = NULL) {
  $message = variable_get('storm_contrib_logger_msg_'.$message_id, '');
  if (empty($message)) {
    $msg_types = storm_contrib_log_heartbeat_message_info();
    $message = $msg_types[$message_id]['message'];
  }
  if (!empty($variables)) {
    $new_vars = array();
    foreach($variables as $var_name => $var) {
      $new_vars['!'.substr($var_name, 1)] = $var;
    }
    $t_message = t($message, $new_vars);
  }
  else {
    $t_message = t($message);
  }
  return $t_message;
}

/**
 * Builds an array with links to the corresponding storm menu.
 *
 * @param array $message - message array from storm_contrib_logger_get_messages()
 * @return array
 *   array with links
 */
function storm_contrib_logger_get_message_action($message) {
  global $user;

  $actions = array();

  switch ($message['message_id']) {
    case 'stormtimetracking_missing':
      if ($message['uid'] == $user->uid) {
        if (empty($user->stormperson_nid)) {
          $actions[] = l(t('Add Timetracking'), 'node/add/stormtimetracking');
        }
        else {
          $actions[] = l(t('View Timetrackings'), 'node/'.$user->stormperson_nid.'/timetrackings');
        }
      }
      else {
        if (empty($message['nid'])) {
          $actions[] = l(t('View User'), 'user/'.$message['uid']);
        }
        else {
          $actions[] = l(t('View Timetrackings'), 'node/'.$message['nid'].'/timetrackings');
        }
      }
      break;
    case 'stormproject_overdue':
    case 'stormproject_costs_exceeded':
      $actions[] = l(t('View Project'), 'node/'.$message['nid']);
      break;

    case 'storm_node_no_assignment':
    case 'storm_node_no_department':
      $actions[] = l(t('View !type', array('!type' => $message['variables']['@node_type'])), 'node/'.$message['nid']);
      if (!empty($message['nid_target'])) {
        $actions[] = l(t('View Project'), 'node/'.$message['nid_target']);
      }
      break;

    case 'stormperson_utlisation_exceeded':
      if (stormperson_utilisation_department_utilisation_access()) {
        $actions[] = l(t('View Utilisation Overview'), 'storm/department-utilisation-by-week');
      }
      $actions[] = l(t('View Person'), 'node/'.$message['nid']);
      break;

  }

  return $actions;
}

/**
 * Retrieves the messages from the database.
 * You can filter the result with the arguments.
 * Arguments can be a id or an array of ids and are combined with OR
 *
 * @param int|array $uid filter by UID
 * @param int|array $uid_target filter by UID target
 * @param int|array $nid filter by NID
 * @param int|array $nid_target filter by NID target
 * @param int $limit limit the result array to this number of results
 * @return array
 *   array of message arrays, each message array have the following keys:
 *     - message_id
 *     - timestamp
 *     - variables
 *     - message
 *     - uid
 *     - nid
 */
function storm_contrib_logger_get_messages($uid = 0, $uid_target = 0, $nid = 0, $nid_target = 0, $limit = 50) {
  global $user;

  $where = array();
  $access_sql = "";
  $args  = array();

  if ($user->uid != 1) {
    $access_message_id = array();
    $msg_types = storm_contrib_log_heartbeat_message_info();
    foreach($msg_types as $type) {
      if (user_access("view ".$type['message_id'])) {
        $access_message_id[] = $type['message_id'];
      }
    }
    if (empty($access_message_id)) {
      return array();
    }
    $access_sql = "message_id IN (".db_placeholders($access_message_id, 'varchar').") AND ";
    $args = array_merge($args, $access_message_id);
  }

  if (!empty($uid)) {
    if (is_array($uid)) {
      $where[] = "uid IN (".db_placeholders($uid, 'int').") ";
      $args[]  = array_merge($args, $uid);
    }
    else {
      $where[] = "uid = %d";
      $args[]  = $uid;
    }
  }
  if (!empty($uid_target)) {
    if (is_array($uid_target)) {
      $where[] = "uid_target IN (".db_placeholders($uid_target, 'int').") ";
      $args[]  = array_merge($args, $uid_target);
    }
    else {
      $where[] = "uid_target = %d";
      $args[]  = $uid_target;
    }
  }
  if (!empty($nid)) {
    if (is_array($nid)) {
      $where[] = "nid IN (".db_placeholders($nid, 'int').") ";
      $args[]  = array_merge($args, $nid);
    }
    else {
      $where[] = "nid = %d";
      $args[]  = $nid;
    }
  }
  if (!empty($nid_target)) {
    if (is_array($nid_target)) {
      $where[] = "nid_target IN (".db_placeholders($nid_target, 'int').") ";
      $args[]  = array_merge($args, $nid_target);
    }
    else {
      $where[] = "nid_target = %d";
      $args[]  = $nid_target;
    }
  }

  $sql   = "SELECT message_id, timestamp, variables, uid, nid FROM {storm_contrib_logs} ";
  if (!empty($where) || !empty($access_sql)) {
    $sql .= "WHERE ".$access_sql."(".implode(' OR ', $where).") ";
  }
  $sql .= tablesort_sql(storm_contrib_logger_view_activities_table_header());

  $return = array();
  $result = pager_query($sql, $limit, 0, NULL, $args);
  while($row = db_fetch_array($result)) {
    $row['variables'] = unserialize($row['variables']);
    $return[] = array(
      'message_id' => $row['message_id'],
      'timestamp'  => $row['timestamp'],
      'variables'  => $row['variables'],
      'message'    => storm_contrib_logger_build_message($row['message_id'], $row['variables']),
      'uid'        => $row['uid'],
      'nid'        => $row['nid'],
    );
  }
  return $return;
}

/**
 * Returns activities table header in the format theme_table() accepts it.
 *
 * @return array
 */
function storm_contrib_logger_view_activities_table_header() {
  return array(
    'message_id' => array(
      'data' => t('Type'),
      'field' => 'message_id'
    ),
    'date' => array(
      'data' => t('Date'),
      'field' => 'timestamp',
      'sort' => 'desc',
    ),
    'message' => array(
      'data' => t('Description'),
      'field' => 'message_id'
    ),
  );
}

/**
 * Outputs the themed activity table.
 *
 * @param null|object $object null for all activities or an stormperson, stormproject or stormorganization node object, to only get corresponding activities
 * @param int $limit
 * @return string
 *   themes html output
 */
function storm_contrib_logger_view_activities($object = NULL, $limit = 50) {
  $content = "";

  $uid = 0; $uid_target = 0; $nid = 0; $nid_target = 0;

  // object is user
  if (isset($object->uid) && isset($object->name) && isset($object->roles)) {
    $uid = $object->uid;
    if (!empty($object->stormperson_nid)) {
      $nid = $object->stormperson_nid;
    }
  }
  // object is node
  elseif(isset($object->nid) && isset($object->title) && isset($object->type)) {
    if ($object->type == 'stormorganization') {
      $uid = array();
      $nid = array();
      $persons = storm_contrib_common_get_stormpersons(array('stormorganization' => $object->nid), 'struct');
      foreach($persons as $person) {
        $uid[] = $person['user_uid'];
        $nid[] = $person['nid'];
      }
    }
    elseif ($object->type == 'stormperson') {
      $nid = $object->nid;
      if (!empty($object->user_uid)) {
        $uid = $object->uid;
      }
    }
    elseif ($object->type == 'stormproject') {
      $nid_target = $object->nid;
      $nid = $object->nid;
    }
  }
  // all activities
  else {
    $uid = 0; $uid_target = 0; $nid = 0; $nid_target = 0;
  }

  $messages = storm_contrib_logger_get_messages($uid, $uid_target, $nid, $nid_target, $limit);
  foreach($messages as &$message) {
    $message['action'] = storm_contrib_logger_get_message_action($message);
  }
  $header = storm_contrib_logger_view_activities_table_header();

  $content .= theme('storm_contib_logger_view_activities_table', $messages, $header, $limit);

  return $content;
}

function storm_contrib_logger_perm() {
  $result = array('view own activities', 'view all activities', 'view activities of organization');

  $messages = storm_contrib_log_heartbeat_message_info();
  foreach($messages as $array) {
    $result[] = "view " . $array['message_id'];
  }

  return $result;
}

/**
 * Access function for the activity view.
 *
 * @param null|object $object null for all activities or an stormperson, stormproject or stormorganization node object, to only get corresponding activities
 * @return bool
 * @see storm_contrib_logger_view_activities()
 */
function storm_contrib_logger_view_activities_access($object = NULL) {
  $access = false;
  global $user;

  // object is user
  if (isset($object->uid) && isset($object->name) && isset($object->roles)) {
    if (user_access('view all activities')) {
      return true;
    }
    if ($user->uid == $object->uid && user_access('view own activities')) {
      return true;
    }
  }
  // object is node
  elseif(isset($object->nid) && isset($object->title) && isset($object->type)) {
    if ($object->type == 'stormorganization') {
      if (user_access('view all activities')) {
        return true;
      }
      $user_node = node_load($user->stormperson_nid);
      if (!empty($user_node->stormorganization_nid)
          && $user_node->stormorganization_nid == $object->nid
          && user_access('view activities of organization')) {
        return true;
      }
    }
    elseif ($object->type == 'stormperson' && !empty($user->stormperson_nid)) {
      if (user_access('view all activities')) {
        return true;
      }
      if ($user->stormperson_nid == $object->nid && user_access('view own activities')) {
        return true;
      }
      $user_node = node_load($user->stormperson_nid);
      if (!empty($user_node->stormorganization_nid)
          && $user_node->stormorganization_nid == $object->stormorganization_nid
          && user_access('view activities of organization')) {
        return true;
      }
    }
    elseif ($object->type == 'stormproject') {
      $user_node = node_load($user->stormperson_nid);
      if (user_access('view all activities')
        || (!empty($user_node->stormorganization_nid)
            && $user_node->stormorganization_nid == $object->stormorganization_nid
            && user_access('view activities of organization'))) {
        return node_access('view', $object);
      }
    }
  }
  // public domain
  else {
    if (user_access('view all activities')) {
      return true;
    }
  }

  return $access;
}

/**
 * Implements hook_storm_contrib_logger_storm_log_write_message().
 * writes or update the activity in the database.
 *
 * @param string $message_id
 * @param int $uid
 * @param int $uid_target
 * @param int $nid
 * @param int $nid_target
 * @param null $variables
 */
function storm_contrib_logger_storm_contrib_logger_storm_log_write_message($message_id, $uid, $uid_target = 0, $nid = 0, $nid_target = 0, $variables = NULL) {

  $object = new stdClass();
  $object->message_id = $message_id;
  $object->uid        = $uid;
  $object->uid_target = $uid_target;
  $object->nid        = $nid;
  $object->nid_target = $nid_target;
  $object->variables  = serialize($variables);
  $object->timestamp  = isset($variables['time']) ? $variables['time'] : time();

  $update = array();
  $types = storm_contrib_log_heartbeat_message_info();
  if (!empty($types[$message_id]['concat_args']['types']) && $types[$message_id]['concat_args']['types'] == 'count') {
    $this_day = mktime(0,0,0);
    $sclid = db_result(db_query("SELECT sclid FROM {storm_contrib_logs} WHERE message_id = '%s' AND uid = %d AND nid = %d AND timestamp > %d", $message_id, $uid, $nid, $this_day));
    if (!empty($sclid)) {
      $object->sclid = $sclid;
      $update[] = 'sclid';
    }
  }

  drupal_write_record('storm_contrib_logs', $object, $update);
}

/**
 * Implements hook_cron().
 * Deletes old entries.
 */
function storm_contrib_logger_cron() {
  $old_messages = variable_get('storm_contrib_logger_delete_older', 2592000);
  if ($old_messages > 0) {
    db_query("DELETE FROM {storm_contrib_logs} WHERE timestamp < %d", time()-$old_messages);
  }
}

/**
 * Implements hook_theme().
 *
 * @return array
 */
function storm_contrib_logger_theme() {
  return array(
    'storm_contrib_logger_message' => array(
      'arguments' => array('message' => array()),
      'file' => 'storm_contrib_logger.theme.inc',
    ),
    'storm_contib_logger_view_activities_table' => array(
      'arguments' => array('message' => array(), 'header' => array(), 'limit' => 50),
      'file' => 'storm_contrib_logger.theme.inc',
    )
  );
}

/**
 * Implements hook_block().
 * Add storm activity table as block.
 *
 * @param string $op
 * @param int $delta
 * @param array $edit
 * @return array
 */
function storm_contrib_logger_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks = array();
    $blocks['table_overview'] = array(
      'info' => t('Storm Activity Table'),
    );
    return $blocks;
  }
  elseif ($op == 'view') {
    $block = array();
    if ($delta == 'table_overview' && user_access('view all activities')) {
      $block['title'] = l(t('All Storm Activities'), 'storm/activities');
      $block['content'] = storm_contrib_logger_view_activities(NULL, 5);
    }
    return $block;
  }
}