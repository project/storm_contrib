<?php

/**
 * @file
 * Provides theme functions for Storm modules
 */

function theme_storm_contrib_filter_blocks_storm_filter_form_group($form) {
  drupal_add_css(drupal_get_path('module', 'storm') .'/storm.css', 'module');
  $output = null; // Variable set as per issue 684016.

  $row = array();
  foreach (element_children($form) as $key) {
    array_push($row, drupal_render($form[$key]));
  }
  $output = '<div id="filter-formgroup-group" class="filter-formgroup-group">';
  foreach($row as $key => $data) {
    $output .= '<div id="filter-formgroup-item" class="filter-formgroup-item" style="float:left;max-width:200px;">'.$data.'</div>';
  }
  $output .= '</div>';
  
  return $output;
}
